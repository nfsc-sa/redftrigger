﻿using REDFTrigger.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace REDFTrigger
{
    partial class REDFTrigger : ServiceBase
    {
        private System.Timers.Timer timer1;
        private System.Timers.Timer timerSupport;
        private string timeString;
        public int getCallType;
        public int getCallTypeSupport;
        string Url = "";
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public int transferDeedREDFDuration;

        public REDFTrigger()
        {
            InitializeComponent();
        }

       

       

       
        protected override void OnStart(string[] args)
        {
            int strTime = Convert.ToInt32(ConfigurationManager.AppSettings["REDFDuration"]);
            getCallType = Convert.ToInt32(ConfigurationManager.AppSettings["REDFDurationType"]);
            int strTimeSupport = Convert.ToInt32(ConfigurationManager.AppSettings["SubsidyPaidDuration"]);
            getCallTypeSupport = Convert.ToInt32(ConfigurationManager.AppSettings["SubsidyPaidType"]);
            transferDeedREDFDuration = Convert.ToInt32(ConfigurationManager.AppSettings["TransferDeedREDFDuration"]);
         
            
            if (getCallType == 1)
            {
                timer1 = new System.Timers.Timer();
                double inter = (double)GetNextInterval();
                log.Info("interval time in Milliseconds" + inter);
                timer1.Interval = inter;
                timer1.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
                
            }
            else
            {
                timer1 = new System.Timers.Timer();
                double inter = strTime * 1000;
                log.Info("interval time in Milliseconds" + inter);
                timer1.Interval = inter;
                timer1.Elapsed += new ElapsedEventHandler(ServiceTimer_Tick);
            }
            if (getCallTypeSupport == 1)
            {
                timerSupport = new System.Timers.Timer();
                double inter = (double)GetNextIntervalSupport();
                log.Info("interval time in Milliseconds" + inter);
                timerSupport.Interval = inter;
                timerSupport.Elapsed += new ElapsedEventHandler(ServiceSupportTimer_Tick);
                //SActivateService();
            }
            else
            {
                timerSupport = new System.Timers.Timer();
                double inter = strTimeSupport * 1000;
                log.Info("interval time in Milliseconds" + inter);
                timerSupport.Interval = inter;
                timerSupport.Elapsed += new ElapsedEventHandler(ServiceSupportTimer_Tick);
            }

            timer1.AutoReset = true;
            timer1.Enabled = true;
            timerSupport.AutoReset = true;
            timerSupport.Enabled = true;
            //SendMailService.WriteErrorLog("Service started");
            log.Info("Service Started");
        }

        private void ServiceSupportTimer_Tick(object sender, ElapsedEventArgs e)
        {
            try
            {
                ///send Post REDFTrigger request
                ActivateSubsidyPaidService();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private void ActivateSubsidyPaidService()
        {
            StringBuilder sbLog = new StringBuilder();
            try
            {
                timerSupport.Stop();
                log.Info("Activated the Subsidy Paid service...");
                log.Info("Subsidy Paid service initialization");
                REDFData.GetSubsidyPaidReq();
            }
            catch (Exception ex)
            {
                log.Error(ex);

            }
            finally
            {
                if (getCallTypeSupport == 1)
                {

                    log.Info("Subsidy Paid Service stoped");
                    System.Threading.Thread.Sleep(1000000);
                    SetTimerSupport();

                }
                else
                {
                    timerSupport.Start();
                    log.Info("Timer started");
                }
            }
        }

        private void ServiceTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {

            try
            {
                ///send Post REDFTrigger request
                ActivateService();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }
        public void OnDebug()
        {
            OnStart(null);
        }
       

        public void ActivateService()
        {

            StringBuilder sbLog = new StringBuilder();
            try
            {
                timer1.Stop();
                log.Info("Activated REDF the service...");

                //Get service configuration value from DB
                // InitializeServiceConfiguration();
                log.Info("Update Status of Contract initialization");
                REDFData.UpdateRedfContractStatus();
                log.Info("IBan Service initialization");
                REDFData.IbanService();
                log.Info("Title deed registration api initilization");
                REDFData.TransferDeedRegistrationService();

                log.Info("Insert Paid Service initialization");
                REDFData.InsertPaidReq();


            }
            catch (Exception ex)
            {
                log.Error(ex);

            }
            finally
            {
                if (getCallType == 1)
                {

                    log.Info(" REDF Service stoped");
                    System.Threading.Thread.Sleep(1000000);
                    SetTimer();

                }
                else
                {
                    timer1.Start();
                    log.Info("Timer started");
                }
            }

        }


       


        private double GetNextInterval()
        {
            timeString = ConfigurationManager.AppSettings["REDFDurationStartTime"];
            DateTime t = DateTime.Parse(timeString);
            TimeSpan ts = new TimeSpan();
            int x;
            ts = t - System.DateTime.Now;

            if (ts.TotalMilliseconds < 0)
            {
                ts = t.AddDays(1) - System.DateTime.Now;//Here you can increase the timer interval based on your requirments.   
            }
            log.Info("Next Interval : " + ts);
            return ts.TotalMilliseconds;
        }
        private double GetNextIntervalSupport()
        {
            timeString = ConfigurationManager.AppSettings["SubsidyPaidStartTime"];
            DateTime t = DateTime.Parse(timeString);
            TimeSpan ts = new TimeSpan();
            int x;
            ts = t - System.DateTime.Now;

            if (ts.TotalMilliseconds < 0)
            {
                ts = t.AddDays(1) - System.DateTime.Now;//Here you can increase the timer interval based on your requirments.   
            }
            log.Info("Next Interval : " + ts);
            return ts.TotalMilliseconds;
        }

        /////////////////////////////////////////////////////////////////////
        private void SetTimer()
        {
            try
            {
                double inter = (double)GetNextInterval();
                timer1.Interval = inter;
                timer1.Start();
                log.Info("Timer started");
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
        private void SetTimerSupport()
        {
            try
            {
                double inter = (double)GetNextIntervalSupport();
                timerSupport.Interval = inter;
                timerSupport.Start();
                log.Info("Timer started");
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }
        private void PostREDFTriggerAPiRequest()
        {

            int flag = 0, count = 0;
            string data = "";

            try
            {
                do
                {
                    var link = string.Format(Url);
                    if (count > 0)
                    {
                        log.Info("retry..." + count);
                    }
                    count++;

                    using (var client = new HttpClient())
                    {
                        ServicePointManager.ServerCertificateValidationCallback = new
                                                    RemoteCertificateValidationCallback
                                                    (
                                                       delegate { return true; }
                                                    );
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        //client.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.Timeout = TimeSpan.FromMinutes(20);
                        var uri = new Uri(link);
                        var Send = client.GetAsync(uri).ContinueWith(task =>
                        {
                            if (task.Status == TaskStatus.RanToCompletion)
                            {
                                var responce = task.Result;
                                if (responce.IsSuccessStatusCode)
                                {
                                    flag = 1;
                                    data = responce.Content.ReadAsStringAsync().Result;
                                    log.Info("response" + data);
                                }
                            }

                        });
                        Send.Wait();
                    }
                } while (count <= 5 && flag == 0);
                //var status = InsertIntoSMSTrack(data, userID, messageTemplate);
            }
            catch (Exception ex)
            {
                var a = ex;
                log.Error(ex);
            }
        }
    }
}
