﻿namespace WindowsServiceProject1
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Prop_REDFTriggerSvc = new System.ServiceProcess.ServiceProcessInstaller();
            this.REDFTriggerSvc = new System.ServiceProcess.ServiceInstaller();
            // 
            // Prop_REDFTriggerSvc
            // 
            this.Prop_REDFTriggerSvc.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.Prop_REDFTriggerSvc.Password = null;
            this.Prop_REDFTriggerSvc.Username = null;
            this.Prop_REDFTriggerSvc.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceProcessInstaller1_AfterInstall);
            // 
            // REDFTriggerSvc
            // 
            this.REDFTriggerSvc.ServiceName = "REDFTrigger";
            this.REDFTriggerSvc.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.REDFTriggerSvc.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.SendBillUpload_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.Prop_REDFTriggerSvc,
            this.REDFTriggerSvc});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller Prop_REDFTriggerSvc;
        private System.ServiceProcess.ServiceInstaller REDFTriggerSvc;
    }
}