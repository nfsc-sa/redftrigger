﻿#region

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

#endregion

namespace REDFTrigger.Helper
{
    /// <summary>
    /// </summary>
    internal sealed class SqlConnectionHelper : IDisposable
    {
        private readonly SqlConnection _connection;

        /// <summary>
        ///     default ctor
        /// </summary>
        public SqlConnectionHelper(string connection)
        {
            try {
            if (_connection == null)
            {
               // _connection = new SqlConnection(ConnectionString(connection));
                _connection= new SqlConnection(ConfigurationManager.ConnectionStrings[connection].ConnectionString);
                _connection.Open();
            }
            }
            catch(Exception  ex)
            {
                throw new System.Exception(  "Unable to connect to the " + connection +Environment.NewLine+ex );
            }
        }

        //Private property for connection string
       


        /// <summary>
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetConnection()
        {
            return _connection;
        }

        /// <summary>
        /// </summary>
        private void Terminate()
        {
            if (_connection != null && _connection.State == ConnectionState.Open)
            {
                _connection.Close();
                _connection.Dispose();
            }
        }

        /// <summary>
        /// </summary>
        ~SqlConnectionHelper()
        {
            Dispose(false);
        }

        #region IDisposable Implementation

        private bool _disposed;

        /// <summary>
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// </summary>
        /// <param name="disposing"></param>
        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Terminate();
                }
                _disposed = true;
            }
        }

        #endregion
    }
}