﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace REDFTrigger.Helper
{
    class SoapRequest
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static string CallWebService(string _url,string _action ,string requeststr )
        {
            //var _url = "http://xxxxxxxxx/Service1.asmx";
            //var _action = "http://xxxxxxxx/Service1.asmx?op=HelloWorld";

            
                //XmlDocument soapEnvelopeDocument = new XmlDocument();
            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(requeststr);
            HttpWebRequest webRequest = CreateWebRequest(_url, _action);
            //  webRequest.Headers[HttpRequestHeader.AcceptEncoding] = "gzip, deflate";
            
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // begin async call to web request.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspend this thread until call is complete. You might want to
            // do something usefull here like update your UI.
            asyncResult.AsyncWaitHandle.WaitOne();

            // get the response from the completed web request.
            string soapResult;
            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }
                log.Info(soapResult);
            }
           // webservicecall()
            return soapResult;
        }
        //public static string webservicecall(string _url, string _action, string requeststr)
        //{

        //    WebRequest req = WebRequest.Create(_url);
        //    HttpWebRequest httpreq = (HttpWebRequest)req;
        //    httpreq.Method = "POST";
        //    httpreq.ContentType = "application/soap+xml; charset=utf-8";
        //    httpreq.Headers.Add("Action:"+ _action);
        //    httpreq.ProtocolVersion = HttpVersion.Version11;
        //    NetworkCredential creds = new NetworkCredential(ConfigurationManager.AppSettings["ReqUserName"], ConfigurationManager.AppSettings["ReqPassword"]);
            
        //    httpreq.Credentials = creds;
        //    Stream str = httpreq.GetRequestStream();
        //    StreamWriter strwriter = new StreamWriter(str, Encoding.ASCII);
        //    strwriter.Write(requeststr);
        //    strwriter.Close();
        //    [] postData=Byte.
        //    httpreq.ContentLength = postData.Length;
        //    using (var dataStream = request.GetRequestStream())
        //    {
        //        dataStream.Write(postData, 0, postData.Length);
        //    }
        //    HttpWebResponse res = (HttpWebResponse)httpreq.GetResponse();
        //    StreamReader rdr = new StreamReader(res.GetResponseStream());
        //    string result = rdr.ReadToEnd();
        //    return result;



        //}

        public static object postRequest4(string destinationUrl,string _action, string requestXml)
        {
            byte[] file = System.Text.Encoding.Unicode.GetBytes(requestXml);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
            request.ContentLength = file.Length;
            request.Method = "POST";
            request.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            request.ContentType = "application/soap+xml";
            request.Headers.Add("Action",_action);
            var byteArray = Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["ReqUserName"] + ":" + ConfigurationManager.AppSettings["ReqPassword"]);
            request.Accept= "application/soap+xml";

            request.Headers.Add("Authorization", "Basic "+ Convert.ToBase64String(byteArray));

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(file, 0, file.Length);
            requestStream.Close();
            HttpWebResponse response;

            response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {

                Stream responseStream = response.GetResponseStream();

                string responseStr = new StreamReader(responseStream).ReadToEnd();

                return responseStr;
            }
            else
            {
                return response;
            }
            //return null;
        }
        public static object postReq5(string destinationUrl, string _action, string requestXml)
        {
            log.Info("Action : " + _action);
            log.Info("URL : " + destinationUrl);
            var client = new RestClient(destinationUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Xml;

            request.AddHeader("Action", _action);
            var PassCode = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["ReqUserName"] + ":" + ConfigurationManager.AppSettings["ReqPassword"]));

            request.AddHeader("Content-Type", "application/soap+xml;charset=UTF-8");
            request.AddHeader("Authorization", "Basic "+PassCode);
            
            request.AddParameter("application/soap+xml;charset=UTF-8", requestXml, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            log.Info("StatusCode: "+response.StatusCode);
            log.Info("Content: " + response.Content);
            log.Info("ErrorMessage: " + response.ErrorMessage);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;
            }
            else
            {
                log.Info(response.ResponseStatus.ToString()+" "+ response.ErrorException);
                log.Error(response.ErrorException);
                return "";
            }
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("Action", action);
            webRequest.ContentType = "application/soap+xml;charset=\"utf-8\"";
            webRequest.Accept = "application/soap+xml";
            webRequest.Method = "POST";
            var PassCode = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["ReqUserName"] + ":" + ConfigurationManager.AppSettings["ReqPassword"]));
            webRequest.Headers.Add("Authorization", "Basic " + PassCode);
            webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            return webRequest;
        }

        private static XmlDocument CreateSoapEnvelope(string xmlstring)
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();
            soapEnvelopeDocument.LoadXml(xmlstring);
            return soapEnvelopeDocument;
        }

        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
        
    }
}
