﻿
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace REDFTrigger.Helper
{
    /// <summary>
    ///     Generic XML parser  class. Serializes/Deserializes the given path and or object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class XMLParser<T> where T : class
    {
        /// <summary>
        ///     Generic XML deserializer. Generates managed class object of given XML file.
        /// </summary>
        /// <param name="path">xml file path as string</param>
        /// <exception cref="">
        ///     Throws exception of Invalid XML value at (0,0) , if missing proper file path.
        /// </exception>
        ///
        /// <returns></returns>
        public static T Deserialize(string path)
        {
            T item;
            var serializer = new XmlSerializer(typeof(T));

            using (var reader = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                item = (T)serializer.Deserialize(reader);

                reader.Flush(true);
                reader.Close();
                reader.Dispose();
            }
            return item;
        }

        /// <summary>
        ///     Serializes managed object to its respective xml file.
        /// </summary>
        /// <typeparam name="T">As Class - Type of object</typeparam>
        /// <param name="item">As Object - class to convert </param>
        /// <param name="path">As String - path where it should get created.</param>
        public static void Serialize(object item, string path)
        {
            var serializer = new XmlSerializer(typeof(T));

            string stringxml = ToXML(item);

            using (var fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                using (var writer = new StreamWriter(fs))
                {
                    serializer.Serialize(writer, item);
                    //Flush to Disk
                    fs.Flush(true);

                    writer.Close();
                    writer.Dispose();
                }
            }
        }
        public static string ToWSDL(object ValidateRequest)
        {
            //ValidateRequest myObject = new ValidateRequest();
            XmlTypeMapping myTypeMapping = (new SoapReflectionImporter().ImportTypeMapping(typeof(T)));
            XmlSerializer mySerializer = new XmlSerializer(myTypeMapping);
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };

            System.Xml.Serialization.XmlSerializerNamespaces namespaces = new System.Xml.Serialization.XmlSerializerNamespaces();
            
            namespaces.Add("soap", "http://www.w3.org/2003/05/soap-envelope");
            namespaces.Add("tem", "http://tempuri.org/");
            namespaces.Add("red", "http://schemas.datacontract.org/2004/07/RedfFinance");
            namespaces.Add("wsa", "http://www.w3.org/2005/08/addressing");

            // System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            //  serializer.Serialize(stream, getBill(), namespaces);

            using (var stringwriter = new System.IO.StringWriter())
            {
                using (var writer = XmlWriter.Create(stringwriter, settings))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    serializer.Serialize(writer, ValidateRequest, namespaces);
                    return stringwriter.ToString();
                }
                
            }
          
        }
        public static string ToWSDLGetSupportReq(object ValidateRequest)
        {
            //ValidateRequest myObject = new ValidateRequest();
            XmlTypeMapping myTypeMapping = (new SoapReflectionImporter().ImportTypeMapping(typeof(T)));
            XmlSerializer mySerializer = new XmlSerializer(myTypeMapping);
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true
            };
            System.Xml.Serialization.XmlSerializerNamespaces namespaces = new System.Xml.Serialization.XmlSerializerNamespaces();
          
            namespaces.Add("soap", "http://www.w3.org/2003/05/soap-envelope");
            namespaces.Add("tem", "http://tempuri.org/");
            namespaces.Add("red", "http://schemas.datacontract.org/2004/07/RedfFinance");
            

            // System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            //  serializer.Serialize(stream, getBill(), namespaces);

            using (var stringwriter = new System.IO.StringWriter())
            {
                using (var writer = XmlWriter.Create(stringwriter, settings))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    serializer.Serialize(writer, ValidateRequest, namespaces);
                    return stringwriter.ToString();
                }
            }
            // StreamWriter myWriter = new StreamWriter("C:/myFileName.xml");
            //   return mySerializer.Serialize(stringwriter, ValidateRequest);
            //myWriter.Close();
            //return "";
        }
        public static string ToWSDLGetSupport(object ValidateRequest)
        {
            //ValidateRequest myObject = new ValidateRequest();
            XmlTypeMapping myTypeMapping = (new SoapReflectionImporter().ImportTypeMapping(typeof(T)));
            XmlSerializer mySerializer = new XmlSerializer(myTypeMapping);

            System.Xml.Serialization.XmlSerializerNamespaces namespaces = new System.Xml.Serialization.XmlSerializerNamespaces();
            namespaces.Add("s", "http://www.w3.org/2003/05/soap-envelope");
            namespaces.Add("a", "http://www.w3.org/2005/08/addressing");
            namespaces.Add("b", "http://schemas.datacontract.org/2004/07/RedfFinance");
            namespaces.Add("i", "http://www.w3.org/2001/XMLSchema-instance");

            // System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            //  serializer.Serialize(stream, getBill(), namespaces);

            using (var stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer((typeof(T)));
                serializer.Serialize(stringwriter, ValidateRequest, namespaces);
                return stringwriter.ToString();
            }
            // StreamWriter myWriter = new StreamWriter("C:/myFileName.xml");
            //   return mySerializer.Serialize(stringwriter, ValidateRequest);
            //myWriter.Close();
            //return "";
        }
        public static string ToWSDL4(object ValidateRequest)
        {
            //ValidateRequest myObject = new ValidateRequest();
            XmlTypeMapping myTypeMapping = (new SoapReflectionImporter().ImportTypeMapping(typeof(T)));
            XmlSerializer mySerializer = new XmlSerializer(myTypeMapping);

            System.Xml.Serialization.XmlSerializerNamespaces namespaces = new System.Xml.Serialization.XmlSerializerNamespaces();
            namespaces.Add("ns", "http://www.BrightWare.com.sa/SADADWare/Upload/1.0");
            namespaces.Add("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
            // System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            //  serializer.Serialize(stream, getBill(), namespaces);

            using (var stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer((typeof(T)));
                serializer.Serialize(stringwriter, ValidateRequest, namespaces);
                return stringwriter.ToString();
            }
            // StreamWriter myWriter = new StreamWriter("C:/myFileName.xml");
            //   return mySerializer.Serialize(stringwriter, ValidateRequest);
            //myWriter.Close();
            //return "";
        }
        public static string ToWSDL2(object ValidateRequest)
        {
            //ValidateRequest myObject = new ValidateRequest();
            XmlTypeMapping myTypeMapping = (new SoapReflectionImporter().ImportTypeMapping(typeof(T)));
            XmlSerializer mySerializer = new XmlSerializer(myTypeMapping);

            System.Xml.Serialization.XmlSerializerNamespaces namespaces = new System.Xml.Serialization.XmlSerializerNamespaces();
            namespaces.Add("sad", "http://www.BrightWare.com.sa/SADADWare");
            namespaces.Add("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
            //namespaces.Add("", "http://www.BrightWare.com.sa/SADADWare/Upload/1.0");
            // System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            //  serializer.Serialize(stream, getBill(), namespaces);

            using (var stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer((typeof(T)));
                serializer.Serialize(stringwriter, ValidateRequest, namespaces);
                return stringwriter.ToString();
            }
            // StreamWriter myWriter = new StreamWriter("C:/myFileName.xml");
            //   return mySerializer.Serialize(stringwriter, ValidateRequest);
            //myWriter.Close();
            //return "";
        }

        public static string ToXML(object item)
        {
            using (var stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer(item.GetType());
                serializer.Serialize(stringwriter, item);
                return stringwriter.ToString();
            }
        }

        public static T LoadFromXMLString(string xmlText)
        {
            using (var stringReader = new System.IO.StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(T));
                return serializer.Deserialize(stringReader) as T;
            }
        }

        //public static T LoadFromXMLString(string xmlText)
        //{
        //    using (var stringReader = new System.IO.StringReader(xmlText))
        //    {
        //        var serializer = new XmlSerializer(typeof(T));
        //        //var t = serializer.Deserialize(stringReader);
        //        //T album = (T)serializer.Deserialize(stringReader);
        //        return serializer.Deserialize(stringReader) as T;
        //    }
        //}


    }
}
