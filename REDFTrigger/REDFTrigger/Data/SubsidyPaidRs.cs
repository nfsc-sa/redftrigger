﻿public class SubsidyPaidRs
{

// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
public partial class Envelope
{

    private EnvelopeHeader headerField;

    private EnvelopeBody bodyField;

    /// <remarks/>
    public EnvelopeHeader Header
    {
        get
        {
            return this.headerField;
        }
        set
        {
            this.headerField = value;
        }
    }

    /// <remarks/>
    public EnvelopeBody Body
    {
        get
        {
            return this.bodyField;
        }
        set
        {
            this.bodyField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
public partial class EnvelopeHeader
{

    private Action actionField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.w3.org/2005/08/addressing")]
    public Action Action
    {
        get
        {
            return this.actionField;
        }
        set
        {
            this.actionField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2005/08/addressing")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2005/08/addressing", IsNullable = false)]
public partial class Action
{

    private byte mustUnderstandField;

    private string valueField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute(Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public byte mustUnderstand
    {
        get
        {
            return this.mustUnderstandField;
        }
        set
        {
            this.mustUnderstandField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTextAttribute()]
    public string Value
    {
        get
        {
            return this.valueField;
        }
        set
        {
            this.valueField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
public partial class EnvelopeBody
{

    private SubsidyPaidResponse subsidyPaidResponseField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://tempuri.org/")]
    public SubsidyPaidResponse SubsidyPaidResponse
    {
        get
        {
            return this.subsidyPaidResponseField;
        }
        set
        {
            this.subsidyPaidResponseField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
public partial class SubsidyPaidResponse
{

    private SubsidyPaidResponseSubsidyPaidResult subsidyPaidResultField;

    /// <remarks/>
    public SubsidyPaidResponseSubsidyPaidResult SubsidyPaidResult
    {
        get
        {
            return this.subsidyPaidResultField;
        }
        set
        {
            this.subsidyPaidResultField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
public partial class SubsidyPaidResponseSubsidyPaidResult
{

    private ResponseCode responseCodeField;

    private SubsidyPaidRespnse subsidyPaidRespnseField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/NFSC.Service.DataContracts")]
    public ResponseCode ResponseCode
    {
        get
        {
            return this.responseCodeField;
        }
        set
        {
            this.responseCodeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/NFSC.Service.DataContracts")]
    public SubsidyPaidRespnse SubsidyPaidRespnse
    {
        get
        {
            return this.subsidyPaidRespnseField;
        }
        set
        {
            this.subsidyPaidRespnseField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/NFSC.Service.DataContracts")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.datacontract.org/2004/07/NFSC.Service.DataContracts", IsNullable = false)]
public partial class ResponseCode
{

    private string resultField;

    private string valueField;

    private string ex_DescriptionField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
    public string Result
    {
        get
        {
            return this.resultField;
        }
        set
        {
            this.resultField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
    public string Value
    {
        get
        {
            return this.valueField;
        }
        set
        {
            this.valueField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
    public string ex_Description
    {
        get
        {
            return this.ex_DescriptionField;
        }
        set
        {
            this.ex_DescriptionField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/NFSC.Service.DataContracts")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.datacontract.org/2004/07/NFSC.Service.DataContracts", IsNullable = false)]
public partial class SubsidyPaidRespnse
{

    private SubsidyPaidRespnseSubsidyPaidRespnse subsidyPaidRespnse1Field;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("SubsidyPaidRespnse")]
    public SubsidyPaidRespnseSubsidyPaidRespnse SubsidyPaidRespnse1
    {
        get
        {
            return this.subsidyPaidRespnse1Field;
        }
        set
        {
            this.subsidyPaidRespnse1Field = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/NFSC.Service.DataContracts")]
public partial class SubsidyPaidRespnseSubsidyPaidRespnse
{

    private string aS_SERIALField;

    private string iBANField;

    private string iBAN_BANK_NAMEField;

    private string nIDField;

    private decimal sUPPORT_VALUEField;

    private string tRANSFER_APROVAL_DATE_MField;

    /// <remarks/>
    public string AS_SERIAL
    {
        get
        {
            return this.aS_SERIALField;
        }
        set
        {
            this.aS_SERIALField = value;
        }
    }

    /// <remarks/>
    public string IBAN
    {
        get
        {
            return this.iBANField;
        }
        set
        {
            this.iBANField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
    public string IBAN_BANK_NAME
    {
        get
        {
            return this.iBAN_BANK_NAMEField;
        }
        set
        {
            this.iBAN_BANK_NAMEField = value;
        }
    }

    /// <remarks/>
    public string NID
    {
        get
        {
            return this.nIDField;
        }
        set
        {
            this.nIDField = value;
        }
    }

    /// <remarks/>
    public decimal SUPPORT_VALUE
    {
        get
        {
            return this.sUPPORT_VALUEField;
        }
        set
        {
            this.sUPPORT_VALUEField = value;
        }
    }

    /// <remarks/>
    public string TRANSFER_APROVAL_DATE_M
    {
        get
        {
            return this.tRANSFER_APROVAL_DATE_MField;
        }
        set
        {
            this.tRANSFER_APROVAL_DATE_MField = value;
        }
    }
}


}