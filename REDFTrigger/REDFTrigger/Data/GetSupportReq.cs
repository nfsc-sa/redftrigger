﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace REDFTrigger.Data
{
    public class GetSupportReq
    {
		// using System.Xml.Serialization;
		// XmlSerializer serializer = new XmlSerializer(typeof(Envelope));
		// using (StringReader reader = new StringReader(xml))
		// {
		//    var test = (Envelope)serializer.Deserialize(reader);
		// }

		[XmlRoot(ElementName = "Header",Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public class Header
		{

			[XmlElement(ElementName = "Action",Namespace = "http://www.w3.org/2005/08/addressing")]
			public string Action { get; set; }

			[XmlAttribute(AttributeName = "wsa")]
			public string Wsa { get; set; }

			[XmlText]
			public string Text { get; set; }
		}

		[XmlRoot(ElementName = "credential",Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public class Credential
		{

			[XmlElement(ElementName = "Password")]
			public string Password { get; set; }

			[XmlElement(ElementName = "UserName")]
			public string UserName { get; set; }
		}

		[XmlRoot(ElementName = "getSupportPaid",Namespace = "http://tempuri.org/")]
		public class GetSupportPaid
		{

			[XmlElement(ElementName = "credential")]
			public Credential Credential { get; set; }

			[XmlElement(ElementName = "F_MONTH")]
			public string FMONTH { get; set; }

			[XmlElement(ElementName = "F_YEAR")]
			public string FYEAR { get; set; }
		}

		[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public class Body
		{

			[XmlElement(ElementName = "getSupportPaid", Namespace = "http://tempuri.org/")]
			public GetSupportPaid GetSupportPaid { get; set; }
		}

		[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public class Envelope
		{

			[XmlElement(ElementName = "Header")]
			public Header Header { get; set; }

			[XmlElement(ElementName = "Body")]
			public Body Body { get; set; }

			[XmlAttribute(AttributeName = "soap")]
			public string Soap { get; set; }

			[XmlAttribute(AttributeName = "tem")]
			public string Tem { get; set; }

			[XmlAttribute(AttributeName = "red")]
			public string Red { get; set; }

			[XmlText]
			public string Text { get; set; }
		}


	}
}