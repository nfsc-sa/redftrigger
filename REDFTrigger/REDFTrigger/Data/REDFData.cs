﻿using REDFTrigger.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using REDFTrigger.Helper;
using System.Configuration;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net;
using System.Net.Security;
using System.Threading.Tasks;
using System.IO;
using REDFTrigger.Models;
using System.Globalization;

namespace REDFTrigger.Data
{
    public class REDFData
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// Iban service :It is the first surface of REDF
        /// it fetch data for DB ... store request and response and send request to 3rd party api and save response 
        /// </summary>
        /// <returns></returns>
        public static void IbanService()
        {
            try
            {

           
            log.Info("Fetch data from for Iban service ");

            DataTable data = SqlHelper.Select("LmsConnection", "GetListForIban", CommandType.StoredProcedure);  //fetch data from DB to process Iban service
            int? count = data?.Rows?.Count;
            log.Info("Number of account to process the request : " + data?.Rows?.Count);
                if (count != null && count <= 0)
                {
                log.Info("No Data available to process the request");
                return;
            }
            List<REDFIban> REDFList = DataTablesOperations.ConvertDataTable<REDFIban>(data);

                foreach (var x in REDFList)
                {
                    Data.IbanServiceReq.Envelope REDFiBanList = GetIbanReqEnvelope(x);
                    log.Info("Saving Iban request in DB");
                    int RequestId = SaveIbanRequestInDB(x);
                    string requestString = XMLParser<Data.IbanServiceReq.Envelope>.ToWSDL(REDFiBanList);
                    log.Info("Request XML" + requestString);

                   var result = IbanReq(requestString);
                    log.Info("Response of post request : ");
                    log.Info(result);
                //Data.IbanServiceRs.Envelope responce = testresult2();
                 //  var result = "testc";
                Data.IbanServiceRs.Envelope responce;


                    if (!string.IsNullOrEmpty(result?.ToString()))
                    {

                      responce = XMLParser<Data.IbanServiceRs.Envelope>.LoadFromXMLString(result.ToString());
                        SaveIbanResponseInDB(responce, RequestId, result.ToString());
                     
                    }

                }


            }
            catch (Exception ex)
            {
                log.Error("Iban Service :" + ex);
            }



        }

        private static void SaveIbanResponseInDB(IbanServiceRs.Envelope responce, int RequestId, string result)
        {
            if (responce?.Body?.insertIBANBanksResponse?.insertIBANBanksResult != null)
            {

                string Description = "";
                if (responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.ex_Description != null)
                {
                    Description = responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.ex_Description;

                }
                var parameters = new[] {
                            new SqlParameter("@RequestId",RequestId),
                            new SqlParameter("@Result",responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.Result),
                            new SqlParameter("@Value",responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.Value),
                            new SqlParameter("@ex_Description",Description),
                            new SqlParameter("@response",result)
                            };
                log.Info("saving response to database");
                var success = SqlHelper.Execute("LmsConnection", "[Usp_InsertIbanResponse]", CommandType.StoredProcedure, parameters) > 0;
                if (success)
                {
                    log.Info("Response saved in database for ");
                }
            }
        }
        private static REDFStatusUpdate.Envelope GetStatusUpdateEnvelope(REDFContractStatus x)
        {
            return new REDFStatusUpdate.Envelope()
            {
                Header = new REDFStatusUpdate.EnvelopeHeader()
                {
                    Action = ConfigurationManager.AppSettings["REDFStatusUpdateAction"]
                },
                Body = new REDFStatusUpdate.EnvelopeBody()
                {
                    UpdateContractStatus = new REDFStatusUpdate.UpdateContractStatus()
                    {
                        statusUpdate_List = new REDFStatusUpdate.StatusUpdate[]
                         {
                             new REDFStatusUpdate.StatusUpdate
                             {
                                  AS_SERIAL = x.AS_SERIAL,
                                  NID = x.NID.ToString() ?? "",
                                  Status_Date=x.Status_Date,
                                  Status_REASON=x.Status_REASON,
                                  Contract_Status=x.Contract_Status


                             }
                         }
                       
                               ,
                        credential = new REDFStatusUpdate.UpdateContractStatusCredential
                        {
                            Password = ConfigurationManager.AppSettings["Password"],
                            UserName = ConfigurationManager.AppSettings["UserName"],


                        },
                    }
                },
            };
        }
        private static IbanServiceReq.Envelope GetIbanReqEnvelope(REDFIban x)
        {
            return new IbanServiceReq.Envelope()
            {
                Header = new IbanServiceReq.EnvelopeHeader()
                {
                    Action = ConfigurationManager.AppSettings["IbanReqAction"]
                },
                Body = new IbanServiceReq.EnvelopeBody()
                {
                    insertIBANBanks = new IbanServiceReq.insertIBANBanks()
                    {
                        IBAN_List = new IbanServiceReq.BanksIBAN[]
                                {
                                     new IbanServiceReq.BanksIBAN
                                        {
                                            AS_SERIAL = x.AS_SERIAL==0 ? "" :x.AS_SERIAL.ToString(),
                                            BANK_CODE = x.BANK_CODE ?? "",
                                           IBAN = x.IBAN ?? "",
                                            FULL_NAME_EN = x.FULL_NAME_EN ?? "",
                                            NID = x.NID.ToString() ?? ""
                                        }
                                }
                               ,
                        credential = new IbanServiceReq.insertIBANBanksCredential()
                        {
                            Password = ConfigurationManager.AppSettings["Password"],
                            UserName = ConfigurationManager.AppSettings["UserName"],


                        },
                    }
                },
            };
        }
        /// <summary>
        /// Update the status of the contract
        /// </summary>
        internal static void UpdateRedfContractStatus()
        {
            try
            {
                SqlParameter[] Parameter = new SqlParameter[]
                {
                new SqlParameter("@PartialPaymentQuoteStatus", ConfigurationManager.AppSettings["PartialPaymentQuoteStatus"])
                };
                DataTable data = SqlHelper.Select("LmsConnection", "Usp_GetRedfContractStatusUpdate", CommandType.StoredProcedure, Parameter);
                int? count = data?.Rows?.Count;
                log.Info("Number of account to process the request : " + data?.Rows?.Count);
                if (count==null || count <= 0)
                {
                    log.Info("No Data available to process the request");
                    return;
                }
                foreach (DataRow row in data.Rows)
                {
                   
                    REDFContractStatus REDF = getReqJson(row);
                    log.Info("Processing request for AS_SERIAL" + REDF.AS_SERIAL);

                    var Req = GetStatusUpdateEnvelope(REDF);

                    string requestString = XMLParser<Data.REDFStatusUpdate.Envelope>.ToWSDL(Req);
                    log.Info("RequestXML");
                    log.Info(requestString);
                    int ContractMapId = int.Parse(row["ID"].ToString());
                    int id = insertRedfStatusReq(requestString, ContractMapId);
                    log.Info("Request saved successfully to DB");
                    string Response = ContractStatusUpdateReq(requestString)?.ToString();
                    log.Info("Response");
                    log.Info(Response);
                    if (!string.IsNullOrEmpty(Response))
                    {
                        insertRedfStatusRes(Response, id, ContractMapId);
                        log.Info("Response saved successfully to DB");
                    }
                   
                }



            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private static REDFContractStatus getReqJson(DataRow row)
        {
          
            return new REDFContractStatus()
            {
                AS_SERIAL = row["AS_SERIAL"].ToString(),
                Contract_Status=row["Contract_Status"].ToString(),
                NID= row["NID"].ToString(),
                Status_Date= row["Status_Date"].ToString(),
                Status_REASON= row["Status_REASON"].ToString(),
                Credential=new Credential()
                {
                    Password = ConfigurationManager.AppSettings["Password"],
                    UserName = ConfigurationManager.AppSettings["UserName"]
                }

            };

        }

        private static int insertRedfStatusReq(string jsonReq,int ContractStatusMappingId)
        {
            SqlParameter[] Parameter = new SqlParameter[]
           {
                new SqlParameter("@Req", jsonReq)
                ,new SqlParameter("@ContractStatusMappingId", ContractStatusMappingId)
       };

            var data = SqlHelper.Scalar("LmsConnection", "[InsertREDFStatusUpdateReq]", CommandType.StoredProcedure, Parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());


        }
        private static void insertRedfStatusRes(string jsonRes,int id,int ContractMapId)
        {
            SqlParameter[] Parameter = new SqlParameter[]
           {
                new SqlParameter("@Res", jsonRes),
                new SqlParameter("@contractMappingId",ContractMapId),
                new SqlParameter("@id",id) };

               
                SqlHelper.Scalar("LmsConnection", "[InsertREDFStatusUpdateRes]", CommandType.StoredProcedure, Parameter);
            

        }


        /// <summary>
        /// Insert paid 
        /// </summary>
        /// <returns></returns>
        public static void InsertPaidReq()
        {
            try
            {

                
                  var Sqlparameters = new[] {
                            new SqlParameter("@RetryCount",ConfigurationManager.AppSettings["RetryCount"])
                            };
                DataTable data = SqlHelper.Select("LmsConnection", "GetListForREDF", CommandType.StoredProcedure, Sqlparameters);
                List<REDFClass> REDFList = DataTableToList(data);
                foreach (var rEDF in REDFList)
               {
                    var item = GetEnvelope(rEDF);
                    var REDFRefNumber = item?.Body?.InsertPaid?.Paid_List?.RealEstatePaid?.AS_SERIAL;
                    int RequestId = InsertRequest(item);
                    string requestString = XMLParser<Data.req.Envelope>.ToWSDL(item);
                    log.Info("Request of post request : ");
                    log.Info(requestString);
                    log.Info("sending api request for REDFRefNumber " + REDFRefNumber);
                    var result = InsertPaidPostReq(requestString);
                    log.Info("Response of post request : ");
                   log.Info(result);
                    Data.Rs.Envelope responce;
                    //Data.Rs.Envelope responce = testresult();
                      //  var result = "testc";

                    if (!string.IsNullOrEmpty(result?.ToString()))
                    {
                       responce = XMLParser<Data.Rs.Envelope>.LoadFromXMLString(result.ToString());
                        if (responce?.Body?.InsertPaidResponse?.InsertPaidResult != null)
                        {

                            string Description = "";
                            if (responce.Body?.InsertPaidResponse?.InsertPaidResult?.Ex_Description != null)
                            {
                                Description = responce.Body?.InsertPaidResponse?.InsertPaidResult?.Ex_Description;

                            }
                            var parameters = new[] {
                          new SqlParameter("@REDFTransactionId", rEDF.Id),
                            new SqlParameter("@RequestId",RequestId),
                            new SqlParameter("@Result",responce.Body?.InsertPaidResponse?.InsertPaidResult.Result),
                            new SqlParameter("@Value",responce.Body?.InsertPaidResponse?.InsertPaidResult.Value),
                            new SqlParameter("@ex_Description",Description),
                            new SqlParameter("@response",result)
                            };
                            log.Info("saving response to database");
                            var success = SqlHelper.Execute("LmsConnection", "[Usp_InsertREDFInsertPaidRs]", CommandType.StoredProcedure, parameters) > 0;
                            if (success)
                            {
                                log.Info("Response saved in database for " + item?.Body?.InsertPaid?.Paid_List?.RealEstatePaid?.AS_SERIAL);
                            }
                        }

                    }
                    else
                    {
                        var parameters = new[] {
                        new SqlParameter("@REDFTransactionId", rEDF.Id) };
                         var success = SqlHelper.Execute("LmsConnection", "Usp_UpdateREDFForFailedStatus", CommandType.StoredProcedure, parameters) > 0;
                        if (success)
                        {
                            log.Info("Failed to send request to REDF");
                        }


                    }



                }
            }
            catch (Exception ex)
            {
                log.Error("InsertPaid : " + ex);
            }
        }

        /// <summary>
        /// Fetch data for subsidy paid Request
        /// </summary>
        /// <returns> List of record to process  </returns>
        internal static void GetSubsidyPaidReq()
        {
            try
            {

            
            SqlParameter[] sqlParameter = new[]
            {
                new SqlParameter("@daycount",ConfigurationManager.AppSettings["SubsidyReqDayCount"])
            };
            DataTable data = SqlHelper.Select("LmsConnection", "GetREDFSubsidyPaid", CommandType.StoredProcedure, sqlParameter);

            foreach (DataRow row in data.Rows)
            {
                ProcessOneRecord(row);
            }
            }
            catch (Exception ex)
            {
                log.Error("SubsidyPaid : " + ex);
            }

        }

        /// <summary>
        /// save request ,send post request and save response
        /// </summary>
        /// <param name="row"> data table row to process </param>
        private static void ProcessOneRecord(DataRow row)
        {
            try
            {

                int REdfTransactionid = int.Parse(row["ID"].ToString());
                string InstallmentNo = row["InstallmentNo"].ToString();
                string REDFRefNumber = row["REDFRefNumber"].ToString();
                int REDFSubsidyPaidReqId = InsertREDFSubsidyPaid(REdfTransactionid, REDFRefNumber, InstallmentNo);

                SubsidyPaidReq.Envelope req = new SubsidyPaidReq.Envelope()
                {
                    Header = new SubsidyPaidReq.EnvelopeHeader()
                    {
                        Action = ConfigurationManager.AppSettings["SubsidyPaidAction"]
                    },

                    Body = new SubsidyPaidReq.EnvelopeBody()
                    {
                        SubsidyPaid = new SubsidyPaidReq.SubsidyPaid()
                        {
                            credential = new SubsidyPaidReq.SubsidyPaidCredential()
                            {
                                Password = ConfigurationManager.AppSettings["Password"],
                                UserName = ConfigurationManager.AppSettings["UserName"],

                            },
                            AS_SERIAL = REDFRefNumber,
                            REPAY_NUM = InstallmentNo


                        }
                    }
                };

                string requestString = XMLParser<Data.SubsidyPaidReq.Envelope>.ToWSDLGetSupportReq(req);
                log.Info("Request of post request : ");
                log.Info(requestString);
                log.Info("sending api request for REDFRefNumber : " + REDFRefNumber + " , installmentNo : " + InstallmentNo);
                var result = SubsidyPaidReq(requestString); //readfile();
                log.Info(result);
                SubsidyPaidRs.Envelope responce;
                
                if (!string.IsNullOrEmpty(result?.ToString()))
                {
                    responce = XMLParser<SubsidyPaidRs.Envelope>.LoadFromXMLString(result.ToString());
                    log.Info(responce);
                    log.Info("saving response to database");
                    SaveSubsidyPaidRsInDB(responce, REDFSubsidyPaidReqId);
                    log.Info("Response saved in database ");


                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
        }

        private static int InsertREDFGetSupport(string v1, string v2)
        {
            SqlParameter[] Parameter = new SqlParameter[]
            {
                new SqlParameter("@Month", v1),
                new SqlParameter("@Year", v1)
        };

            var data = SqlHelper.Scalar("LmsConnection", "InsertREDFGetSupportReq", CommandType.StoredProcedure, Parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());


        }

        /// <summary>
        /// save  insert paid request in db
        /// </summary>
        /// <param name="id"> redf transaction id</param>
        /// <param name="v1">REDFRefNumber</param>
        /// <param name="v2">InstallmentNo</param>
        /// <returns> return id of inserted row</returns>
        private static int InsertREDFSubsidyPaid(int id, string v1, string v2)
        {
            SqlParameter[] Parameter = new SqlParameter[]
            {
                new SqlParameter("@Id", id),
                new SqlParameter("@MortgageLoanAccountNumber", v1),
                 new SqlParameter("@InstallmentNo", v2)
        };

            var data = SqlHelper.Scalar("LmsConnection", "InsertREDFSubsidyPaidReq", CommandType.StoredProcedure, Parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());


        }
        /// <summary>
        /// save subsidy paid response in DB
        /// </summary>
        /// <param name="dt"> request xml</param>
        /// <param name="Id">Database ID for request xml</param>
        private static void SaveSubsidyPaidRsInDB(SubsidyPaidRs.Envelope dt, int Id)
        {
            DateTime? temp = null;
            string supportDate = dt.Body?.SubsidyPaidResponse?.SubsidyPaidResult?.SubsidyPaidRespnse?.SubsidyPaidRespnse1?.TRANSFER_APROVAL_DATE_M;
            if (!string.IsNullOrEmpty(supportDate))
            {
                temp = DateTime.ParseExact(supportDate, "yyyyMMdd", CultureInfo.InvariantCulture);
            }

            List<SqlParameter> Parameter = new List<SqlParameter>
           {
                  new SqlParameter("@Result" ,dt.Body?.SubsidyPaidResponse.SubsidyPaidResult?.ResponseCode?.Result??""),
                  new SqlParameter("@Value" ,dt.Body?.SubsidyPaidResponse.SubsidyPaidResult?.ResponseCode?.Value??""),
                  new SqlParameter("@ex_Description" ,dt.Body.SubsidyPaidResponse?.SubsidyPaidResult?.ResponseCode?.ex_Description??""),
                  new SqlParameter("@AS_SERIAL" ,dt.Body.SubsidyPaidResponse?.SubsidyPaidResult?.SubsidyPaidRespnse?.SubsidyPaidRespnse1?.AS_SERIAL??""),
                  new SqlParameter("@IBAN",dt.Body.SubsidyPaidResponse?.SubsidyPaidResult?.SubsidyPaidRespnse?.SubsidyPaidRespnse1?.IBAN??"")
                 ,new SqlParameter("@IBAN_BANK_NAME",dt.Body.SubsidyPaidResponse?.SubsidyPaidResult?.SubsidyPaidRespnse?.SubsidyPaidRespnse1?.IBAN_BANK_NAME??"")
                 ,new SqlParameter("@NID",dt.Body.SubsidyPaidResponse?.SubsidyPaidResult?.SubsidyPaidRespnse?.SubsidyPaidRespnse1?.NID??"")
                 ,new SqlParameter("@SUPPORT_VALUE",dt.Body.SubsidyPaidResponse?.SubsidyPaidResult?.SubsidyPaidRespnse?.SubsidyPaidRespnse1?.SUPPORT_VALUE??0)
                 
                 ,new SqlParameter("@RequestId",Id),

               };
            if (temp != null)
            {
                Parameter.Add(new SqlParameter("@TRANSFER_APPROVAL_DATE_M", temp));

            }
            SqlHelper.Execute("LmsConnection", "InsertSubsidyPaidRs", CommandType.StoredProcedure, Parameter.ToArray());
        }
      /// <summary>
      /// to testing purpose in Local
      /// </summary>
      /// <returns></returns>
        public static object readfile()
        {
            return File.ReadAllText(@"C:\Users\Diva Smriti\Desktop\Data2.xml");                  //File.ReadAllText(@"C:\Users\Diva Smriti\Desktop\Res2.xml");
        }

       /// <summary>
       /// save the Iban request
       /// </summary>
       /// <param name="dt"></param>
        private static int SaveIbanRequestInDB(REDFIban dt)
        {
            
                
                    //SaveRea
                
            SqlParameter[] Parameter = new SqlParameter[]
            {
                new SqlParameter("@AS_SERIAL", dt.AS_SERIAL),
                new SqlParameter("@BANK_CODE", dt.BANK_CODE),
                 new SqlParameter("@IBAN", dt.IBAN),
                  new SqlParameter("@FULL_NAME_EN", dt.FULL_NAME_EN),
                new SqlParameter("@NID", dt.NID),
                 new SqlParameter("@REDFTransactionid", dt.REDFTransactionid)
            };

            var data = SqlHelper.Scalar("LmsConnection", "InsertIBanReq", CommandType.StoredProcedure, Parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());




        }

        private static DataTable GetibanRsTable(int Id, IbanServiceReq.Envelope responce)
        {
            System.Guid guid = new Guid();
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("iBanReq");
            dt.Columns.Add("AS_SERIAL");
            dt.Columns.Add("BANK_CODE");
            dt.Columns.Add("IBAN");
            dt.Columns.Add("FULL_NAME_EN");
            dt.Columns.Add("REDFTransactionid");
           
            dt.Columns.Add("TRANSFER_APPROVAL_DATE_M");
            dt.Columns.Add("RequestId");
            foreach (var t in responce.Body.insertIBANBanks.IBAN_List)
            {
                DataRow _dr = dt.NewRow();
                _dr["iBanReq"] = guid;
                _dr["AS_SERIAL"] = t.AS_SERIAL;
                _dr["BANK_CODE"] = t.BANK_CODE;
                _dr["IBAN"] = t.IBAN;
                _dr["FULL_NAME_EN"] = t.FULL_NAME_EN;
                _dr["REDFTransactionid"] = Id;
                dt.Rows.Add(_dr);

            }
            return dt;
        }

        private static Rs.Envelope testresult()
        {
            Data.Rs.Envelope responce = new Rs.Envelope()
            {
                Body = new Rs.Body
                {
                    InsertPaidResponse = new Rs.InsertPaidResponse()
                    {
                        InsertPaidResult = new Rs.InsertPaidResult()
                        {
                            Result = "test",
                            Value = "1000",
                            Ex_Description = "test"
                        }
                    }
                }
            };
            return responce;
        }
        private static GetSupportRs.Envelope testresult1()
        {
            Data.GetSupportRs.Envelope responce = new GetSupportRs.Envelope()
            {
                Body = new GetSupportRs.Body
                {
                    GetSupportPaidResponse = new GetSupportRs.GetSupportPaidResponse()
                    {
                        GetSupportPaidResult = new GetSupportRs.GetSupportPaidResult()
                        {
                            ResSuppPaidList = new GetSupportRs.ResSuppPaidList()
                            {
                                RealEstateSupportPaid = new List<GetSupportRs.RealEstateSupportPaid>()
                                {
                                    new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd",
                                        ASSERIAL="125518912110",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=6,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    },
                                    new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd1",
                                        ASSERIAL="125518912110",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=7,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    },
                                    new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd1",
                                        ASSERIAL="168583261",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=15,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    },
                                     new GetSupportRs.RealEstateSupportPaid()
                                    {
                                        FULLNAMEEN="sadasd1",
                                        ASSERIAL="168583261",
                                        IBAN="sa",
                                        SUPPORTVALUE=123.80,
                                        REPAYNUM=16,
                                        TRANSFERAPPROVALDATEM="as",
                                        NID="As"
                                    }
                                }
                            }
                        }
                    }
                }
            };
            return responce;
        }
        private static IbanServiceRs.Envelope testresult2()
        {
            Data.IbanServiceRs.Envelope responce = new IbanServiceRs.Envelope()
            {
                Body = new IbanServiceRs.EnvelopeBody()
               {
                    insertIBANBanksResponse = new IbanServiceRs.insertIBANBanksResponse()
                   {
                        insertIBANBanksResult = new IbanServiceRs.insertIBANBanksResponseInsertIBANBanksResult()
                       {
                            ex_Description = "data ex",
                            Result = "error",
                            Value = "1000"
                       }
                   }
               }
            };
            return responce;
        }
        /// <summary>
        /// Insert paid Post request to 3rd party
        /// </summary>
        /// <param name="requestXml"></param>
        /// <returns>3rd party response  </returns>
        public static object InsertPaidPostReq(string requestXml)
        {
            string destinationUrl = ConfigurationManager.AppSettings["REDFPostUrl"];
            string actionName = ConfigurationManager.AppSettings["InsertPaidAction"];
            return SoapRequest.postReq5(destinationUrl, actionName, requestXml);

        }
        public static object ContractStatusUpdateReq(string requestXml)
        {
            string destinationUrl = ConfigurationManager.AppSettings["REDFPostUrl"];
            string actionName = ConfigurationManager.AppSettings["REDFStatusUpdateAction"];
            return SoapRequest.postReq5(destinationUrl, actionName, requestXml);

        }

        /// <summary>
        /// iBan service post request to 3rd party
        /// </summary>
        /// <param name="requestXml"></param>
        /// <returns>xml response as string</returns>
        public static object IbanReq(string requestXml)
        {
            string destinationUrl = ConfigurationManager.AppSettings["REDFPostUrl"];
            string actionName = ConfigurationManager.AppSettings["IbanReqAction"];
            
            return SoapRequest.postReq5(destinationUrl, actionName, requestXml);

        }

        /// <summary>
        /// SubsidyPaid service post request to 3rd party
        /// </summary>
        /// <param name="requestXml"></param>
        /// <returns>xml response as string</returns>
        public static object SubsidyPaidReq(string requestXml)
        {
            string destinationUrl = ConfigurationManager.AppSettings["REDFPostUrl"];
            string actionName = ConfigurationManager.AppSettings["SubsidyPaidAction"];
            //string fileName = @"C:\Users\TareqAAlmokhadab\Downloads\test.xml";
            // byte[] file = File.ReadAllBytes(requestXml);
           
           return SoapRequest.postReq5(destinationUrl, actionName, requestXml);
            
            //return null;
        }

        private void PostAPiRequest()
        {

            int flag = 0, count = 0;
            string data = "";

            try
            {
                do
                {
                    if (count > 0)
                    {
                        log.Info("retry..." + count);
                    }
                    count++;

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["REDFPostUrl"]);
                        ServicePointManager.ServerCertificateValidationCallback = new
                                                    RemoteCertificateValidationCallback
                                                    (
                                                       delegate { return true; }
                                                    );
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        //client.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.Timeout = TimeSpan.FromMinutes(20);
                        var uri = new Uri(string.Format("/id={0}&type={1}", "dfg", "sdf"));
                        var Send = client.GetAsync(uri).ContinueWith(task =>
                        {
                            if (task.Status == TaskStatus.RanToCompletion)
                            {
                                var responce = task.Result;
                                if (responce.IsSuccessStatusCode)
                                {
                                    flag = 1;
                                    data = responce.Content.ReadAsStringAsync().Result;
                                    log.Info("response" + data);
                                }
                            }

                        });
                        Send.Wait();
                    }
                } while (count <= 5 && flag == 0);
                //var status = InsertIntoSMSTrack(data, userID, messageTemplate);
            }
            catch (Exception ex)
            {
                var a = ex;
                log.Error(ex);
            }
        }
        /// <summary>
        /// save insert paid request 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static int InsertRequest(req.Envelope item)
        {
            double PAY_AMOUNT = string.IsNullOrEmpty(item.Body.InsertPaid.Paid_List.RealEstatePaid.PAY_AMOUNT) ? 0 : double.Parse(item.Body.InsertPaid.Paid_List.RealEstatePaid.PAY_AMOUNT);
            var parameter = new[] {
                new SqlParameter("@AS_SERIAL",item.Body.InsertPaid.Paid_List.RealEstatePaid.AS_SERIAL),
                new SqlParameter("@REPAY_NUM",item.Body.InsertPaid.Paid_List.RealEstatePaid.REPAY_NUM),

                new SqlParameter("@PAY_AMOUNT",PAY_AMOUNT),

                new SqlParameter("@PAY_DATE_M",item.Body.InsertPaid.Paid_List.RealEstatePaid.PAY_DATE_M),
                new SqlParameter("@NID",item.Body.InsertPaid.Paid_List.RealEstatePaid.NID)
                         };
            var data = SqlHelper.Scalar("LmsConnection", "Usp_InsertREDFInsertPaidReq", CommandType.StoredProcedure, parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());
        }

        /// <summary>
        /// convert datatable to list 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
      
        private static List<REDFClass> DataTableToList(DataTable data)
        {
            List<REDFClass> BillRecord = new List<REDFClass>();
            foreach (DataRow row in data.Rows)
            {
                
                REDFClass rEDF = new REDFClass()
                {
                    Id = int.Parse(row["Id"]?.ToString()),
                    REDFRefNumber = row["REDFRefNumber"].ToString(),
                    NationalID = row["NationalID"].ToString(),
                    InstallmentAmount = row["InstallmentAmount"].ToString(),
                    TransactionDate = row["TransactionDate"].ToString(),
                    InstallmentNo = row["InstallmentNo"].ToString()
                };
                BillRecord.Add(rEDF);

            }
            return BillRecord;
        }

        /// <summary>
        /// get xml object
        /// </summary>
        /// <param name="rEDF"></param>
        /// <returns></returns>
        public static Data.req.Envelope GetEnvelope(REDFClass rEDF)
        {
            Data.req.Envelope billRecord_Type = new Data.req.Envelope()
            {

                Header = new req.Header()
                {
                    Action = ConfigurationManager.AppSettings["InsertPaidAction"]
                },
                Body = new req.Body()
                {
                    InsertPaid = new req.InsertPaid()
                    {
                        Credential = new req.Credential()
                        {
                            Password = ConfigurationManager.AppSettings["Password"],
                            UserName = ConfigurationManager.AppSettings["UserName"],


                        },
                        Paid_List = new req.Paid_List()
                        {
                            RealEstatePaid = new req.RealEstatePaid()
                            {
                                AS_SERIAL = rEDF.REDFRefNumber,
                                NID = rEDF.NationalID,
                                PAY_AMOUNT = rEDF.InstallmentAmount,
                                PAY_DATE_M = rEDF.TransactionDate,
                                REPAY_NUM = rEDF.InstallmentNo        //Which installment number is paid by customer
                            }
                        }

                    }
                }

            };

            return billRecord_Type;

        }

        public static void TransferDeedRegistrationService()
        {
            try
            {


                log.Info("Fetch data from for TransferDeedRegistration service ");

                DataTable data = SqlHelper.Select("LmsConnection", "uspGetTitleDeedTransferedData", CommandType.StoredProcedure);  //fetch data from DB to process Iban service
                int? count = data?.Rows?.Count;
                log.Info("Number Title Deed to process the request : " + data?.Rows?.Count);
                if (count != null && count <= 0)
                {
                    log.Info("No Data available to process the request");
                    return;
                }
                
                List<TDREDFRegistration>TDRegREDFList = DataTablesOperations.ConvertDataTable<TDREDFRegistration>(data);
                log.Info("After converting into list");
                foreach (var x in TDRegREDFList)
                {
                    Data.TitleDeedRegistrationServiceReq.Envelope TDRegList = GetTDRegReqEnvelope(x);
                    log.Info("Saving TransferDeedRegistrationService request in DB");
                    int RequestId = SaveTDRegRequestInDB(x);
                    string requestString = XMLParser<Data.TitleDeedRegistrationServiceReq.Envelope>.ToWSDL(TDRegList);
                    log.Info("Request XML" + requestString);
                    var result = TDReq(requestString);
                    log.Info("Response of post request : ");
                    log.Info(result);
                    //Data.IbanServiceRs.Envelope responce = testresult2();
                    //  var result = "testc";
                    Data.IbanServiceRs.Envelope responce;


                    if (!string.IsNullOrEmpty(result?.ToString()))
                    {

                        responce = XMLParser<Data.IbanServiceRs.Envelope>.LoadFromXMLString(result.ToString());
                        SaveTDRegistrationResponseInDB(responce, RequestId, result.ToString());

                        log.Info("After saving TD REDF response to database");

                        var parameters = new[] {
                            new SqlParameter("@ID",x.ID) };
                        var success = SqlHelper.Execute("LmsConnection", "uspUpdateTransferDeedStatusREDF", CommandType.StoredProcedure, parameters) > 0;
                        if (success)
                        {
                            log.Info("After updating TD REDF response to database");
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                log.Error("TransferDeedRegistrationService Service :" + ex);
            }



        }
     
        private static void SaveTDRegistrationResponseInDB(IbanServiceRs.Envelope responce, int RequestId, string result)
        {
            if (responce?.Body?.insertIBANBanksResponse?.insertIBANBanksResult != null)
            {

                string Description = "";
                if (responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.ex_Description != null)
                {
                    Description = responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.ex_Description;

                }
                var parameters = new[] {
                            new SqlParameter("@RequestId",RequestId),
                            new SqlParameter("@Result",responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.Result),
                            new SqlParameter("@Value",responce.Body?.insertIBANBanksResponse?.insertIBANBanksResult?.Value),
                            new SqlParameter("@ex_Description",Description),
                            new SqlParameter("@response",result)
                            };
                log.Info("saving response to database");
                var success = SqlHelper.Execute("LmsConnection", "[Usp_InsertTDRegResponse]", CommandType.StoredProcedure, parameters) > 0;
                if (success)
                {
                    log.Info("Response saved in database for ");
                }
            }
        }

        private static TitleDeedRegistrationServiceReq.Envelope GetTDRegReqEnvelope(TDREDFRegistration x)
        {
            return new TitleDeedRegistrationServiceReq.Envelope()
            {
                Header = new TitleDeedRegistrationServiceReq.EnvelopeHeader()
                {
                    Action = ConfigurationManager.AppSettings["TitleDeedReqAction"]
                },
                Body = new TitleDeedRegistrationServiceReq.EnvelopeBody()
                {
                    insertTDRegistration = new TitleDeedRegistrationServiceReq.insertTDRegistration()
                    {
                        TDRegistration_List = new TitleDeedRegistrationServiceReq.TDRegistration[]
                                {
                                     new TitleDeedRegistrationServiceReq.TDRegistration
                                        {
                                            AS_SERIAL = x.REDFRefNumber,
                                            AreaOfLand = x.AreaOfLand ,
                                            AreaOfProperty = x.AreaOfProperty ,
                                            BuildingType = x.PropertyTypeID,
                                            LandNumberField = x.LandNumber ,
                                            Latitude = x.Latitude,
                                            Longitude = x.Longitude,
                                            RealEstateSubDivisionNumber= x.RealEstateSubDivisionNumber,
                                         TitleDeedDate = Convert.ToString(x.TransferDate),
                                         TitleDeedNumber = x.TitleDeedNo
                                        }
                                }
                               ,
                        credential = new TitleDeedRegistrationServiceReq.insertTDRegistrationCredential()
                        {
                            Password = ConfigurationManager.AppSettings["Password"],
                            UserName = ConfigurationManager.AppSettings["UserName"],


                        },
                    }
                },
            };
        }

        private static int SaveTDRegRequestInDB(TDREDFRegistration dt)
        {


            //SaveRea

            SqlParameter[] Parameter = new SqlParameter[]
            {
                new SqlParameter("@AS_SERIAL", dt.REDFRefNumber),
                new SqlParameter("@RealEstateSubDivisionNumber", dt.RealEstateSubDivisionNumber),
                 new SqlParameter("@LandNumber", dt.LandNumber),
                  new SqlParameter("@AreaOfLand", dt.AreaOfLand),
                new SqlParameter("@AreaOfProperty", dt.AreaOfProperty),
                 new SqlParameter("@BuildingType", dt.PropertyTypeID),
                 new SqlParameter("@TitleDeedNumber", dt.TitleDeedNo),
                 new SqlParameter("@TitleDeedTransferDate", dt.TransferDate),
                 new SqlParameter("@Latitude", dt.Latitude),
                 new SqlParameter("@Longitude", dt.Longitude)

            };

            var data = SqlHelper.Scalar("LmsConnection", "uspInsertTransferDeedREDFRequest", CommandType.StoredProcedure, Parameter);
            if (data == null)
            {
                return -1;
            }
            return int.Parse(data.ToString());




        }

        /// <summary>
        /// TD service post request to 3rd party
        /// </summary>
        /// <param name="requestXml"></param>
        /// <returns>xml response as string</returns>
        public static object TDReq(string requestXml)
        {
            string destinationUrl = ConfigurationManager.AppSettings["REDFPostUrl"];
            string actionName = ConfigurationManager.AppSettings["TitleDeedReqAction"];

            return SoapRequest.postReq5(destinationUrl, actionName, requestXml);

        }
    }
}