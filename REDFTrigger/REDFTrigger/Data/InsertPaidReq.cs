﻿
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace REDFTrigger.Data.req
{
	[XmlRoot(ElementName = "Header", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class Header
	{
		[XmlElement(ElementName = "Action", Namespace = "http://www.w3.org/2005/08/addressing")]
		public string Action { get; set; }
		[XmlAttribute(AttributeName = "wsa", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Wsa { get; set; }
	}

	[XmlRoot(ElementName = "RealEstatePaid", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
	public class RealEstatePaid
	{
		[XmlElement(ElementName = "AS_SERIAL", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string AS_SERIAL { get; set; }
		[XmlElement(ElementName = "NID", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string NID { get; set; }
		[XmlElement(ElementName = "PAY_AMOUNT", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string PAY_AMOUNT { get; set; }
		[XmlElement(ElementName = "PAY_DATE_M", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string PAY_DATE_M { get; set; }
		[XmlElement(ElementName = "REPAY_NUM", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string REPAY_NUM { get; set; }
	}

	[XmlRoot(ElementName = "Paid_List", Namespace = "http://tempuri.org/")]
	public class Paid_List
	{
		[XmlElement(ElementName = "RealEstatePaid", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public RealEstatePaid RealEstatePaid { get; set; }
	}

	[XmlRoot(ElementName = "credential", Namespace = "http://tempuri.org/")]
	public class Credential
	{
		[XmlElement(ElementName = "Password", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string Password { get; set; }
		[XmlElement(ElementName = "UserName", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string UserName { get; set; }
	}

	[XmlRoot(ElementName = "insertPaid", Namespace = "http://tempuri.org/")]
	public class InsertPaid
	{
		[XmlElement(ElementName = "Paid_List", Namespace = "http://tempuri.org/")]
		public Paid_List Paid_List { get; set; }
		[XmlElement(ElementName = "credential", Namespace = "http://tempuri.org/")]
		public Credential Credential { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class Body
	{
		[XmlElement(ElementName = "insertPaid", Namespace = "http://tempuri.org/")]
		public InsertPaid InsertPaid { get; set; }
	}

	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class Envelope
	{
		[XmlElement(ElementName = "Header", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public Header Header { get; set; }
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public Body Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "tem", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Tem { get; set; }
		[XmlAttribute(AttributeName = "red", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Red { get; set; }
	}

}

