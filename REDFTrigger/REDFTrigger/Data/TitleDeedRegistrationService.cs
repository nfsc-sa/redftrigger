﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REDFTrigger.Data
{
    public class TitleDeedRegistrationServiceReq
    {
        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
        public partial class Envelope
        {


            private EnvelopeHeader headerField;

            private EnvelopeBody bodyField;

            /// <remarks/>
            public EnvelopeHeader Header
            {
                get
                {
                    return this.headerField;
                }
                set
                {
                    this.headerField = value;
                }
            }


            /// <remarks/>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        public partial class EnvelopeHeader
        {

            private string actionField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.w3.org/2005/08/addressing")]
            public string Action
            {
                get
                {
                    return this.actionField;
                }
                set
                {
                    this.actionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance", IsNullable = false)]
        public partial class TDRegistration
        {

            private string aS_SERIALField;

            private string realEstateSubDivisionNumberField;

            private string landNumberField;

            private string areaOfLandField;

            private string areaOfPropertyField;

            private string buildingTypeField;

            private string titleDeedNumberField;

            private string titleDeedDateField;

            private string latitudeField;
            private string longitudeField;

            /// <remarks/>
            public string AS_SERIAL
            {
                get
                {
                    return this.aS_SERIALField;
                }
                set
                {
                    this.aS_SERIALField = value;
                }
            }

            /// <remarks/>
            public string RealEstateSubDivisionNumber
            {
                get
                {
                    return this.realEstateSubDivisionNumberField;
                }
                set
                {
                    this.realEstateSubDivisionNumberField = value;
                }
            }

            /// <remarks/>
            public string LandNumberField
            {
                get
                {
                    return this.landNumberField;
                }
                set
                {
                    this.landNumberField = value;
                }
            }

            /// <remarks/>
            public string AreaOfLand
            {
                get
                {
                    return this.areaOfLandField;
                }
                set
                {
                    this.areaOfLandField = value;
                }
            }

            /// <remarks/>
            public string AreaOfProperty
            {
                get
                {
                    return this.areaOfPropertyField;
                }
                set
                {
                    this.areaOfPropertyField = value;
                }
            }
            public string BuildingType
            {
                get
                {
                    return this.buildingTypeField;
                }
                set
                {
                    this.buildingTypeField = value;
                }
            }
            public string TitleDeedNumber
            {
                get
                {
                    return this.titleDeedNumberField;
                }
                set
                {
                    this.titleDeedNumberField = value;
                }
            }
            public string TitleDeedDate
            {
                get
                {
                    return this.titleDeedDateField;
                }
                set
                {
                    this.titleDeedDateField = value;
                }
            }
            public string Latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }
            public string Longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        public partial class EnvelopeBody
        {

            private insertTDRegistration insertTDRegistrationField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://tempuri.org/")]
            public insertTDRegistration insertTDRegistration
            {
                get
                {
                    return this.insertTDRegistrationField;
                }
                set
                {
                    this.insertTDRegistrationField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
        public partial class insertTDRegistration
        {

            private insertTDRegistrationCredential credentialField;

            private TDRegistration[] tdRegistration_ListField;

            /// <remarks/>
            public insertTDRegistrationCredential credential
            {
                get
                {
                    return this.credentialField;
                }
                set
                {
                    this.credentialField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("BanksIBAN", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance", IsNullable = false)]
            public TDRegistration[] TDRegistration_List
            {
                get
                {
                    return this.tdRegistration_ListField;
                }
                set
                {
                    this.tdRegistration_ListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
        public partial class insertTDRegistrationCredential
        {

            private string passwordField;

            private string userNameField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
            public string Password
            {
                get
                {
                    return this.passwordField;
                }
                set
                {
                    this.passwordField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
            public string UserName
            {
                get
                {
                    return this.userNameField;
                }
                set
                {
                    this.userNameField = value;
                }
            }
        }
    }
}
