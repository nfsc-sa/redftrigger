﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REDFTrigger.Data
{
    public class REDFStatusUpdate
    {
       

            // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
            /// <remarks/>
            [System.SerializableAttribute()]
            [System.ComponentModel.DesignerCategoryAttribute("code")]
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
            [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
            public partial class Envelope
            {


                private EnvelopeHeader headerField;

                private EnvelopeBody bodyField;

                /// <remarks/>
                public EnvelopeHeader Header
                {
                    get
                    {
                        return this.headerField;
                    }
                    set
                    {
                        this.headerField = value;
                    }
                }


                /// <remarks/>
                public EnvelopeBody Body
                {
                    get
                    {
                        return this.bodyField;
                    }
                    set
                    {
                        this.bodyField = value;
                    }
                }
            }
            [System.SerializableAttribute()]
            [System.ComponentModel.DesignerCategoryAttribute("code")]
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
            public partial class EnvelopeHeader
            {

                private string actionField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.w3.org/2005/08/addressing")]
                public string Action
                {
                    get
                    {
                        return this.actionField;
                    }
                    set
                    {
                        this.actionField = value;
                    }
                }
            }

            /// <remarks/>
            [System.SerializableAttribute()]
            [System.ComponentModel.DesignerCategoryAttribute("code")]
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
            [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance", IsNullable = false)]
            public partial class StatusUpdate
            {

                private string aS_SERIALField;

                private string contract_Status;

                private string status_REASON;

                private string status_Date;

                private string nIDField;

                /// <remarks/>
                public string AS_SERIAL
                {
                    get
                    {
                        return this.aS_SERIALField;
                    }
                    set
                    {
                        this.aS_SERIALField = value;
                    }
                }
                public string NID
                {
                    get
                    {
                        return this.nIDField;
                    }
                    set
                    {
                        this.nIDField = value;
                    }
                }

            /// <remarks/>
                public string Contract_Status
                {
                        get
                        {
                            return this.contract_Status;
                        }
                        set
                        {
                            this.contract_Status = value;
                        }
                    }

                /// <remarks/>
                public string Status_REASON
            {
                    get
                    {
                        return this.status_REASON;
                    }
                    set
                    {
                        this.status_REASON = value;
                    }
                }

                /// <remarks/>
                public string Status_Date
            {
                    get
                    {
                        return this.status_Date;
                    }
                    set
                    {
                        this.status_Date = value;
                    }
                }

                /// <remarks/>
                
            }

            /// <remarks/>
            [System.SerializableAttribute()]
            [System.ComponentModel.DesignerCategoryAttribute("code")]
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
            public partial class EnvelopeBody
            {

                private UpdateContractStatus UpdateContractStatusField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://tempuri.org/")]
                public UpdateContractStatus UpdateContractStatus
                {
                    get
                    {
                        return this.UpdateContractStatusField;
                    }
                    set
                    {
                        this.UpdateContractStatusField = value;
                    }
                }
            }

            /// <remarks/>
            [System.SerializableAttribute()]
            [System.ComponentModel.DesignerCategoryAttribute("code")]
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
            [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
            public partial class UpdateContractStatus
            {

                private UpdateContractStatusCredential credentialField;

                private StatusUpdate[] statusUpdate_ListField;

                /// <remarks/>
                public UpdateContractStatusCredential credential
                {
                    get
                    {
                        return this.credentialField;
                    }
                    set
                    {
                        this.credentialField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlArrayItemAttribute("BanksstatusUpdate", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance", IsNullable = false)]
                public StatusUpdate[] statusUpdate_List
                {
                    get
                    {
                        return this.statusUpdate_ListField;
                    }
                    set
                    {
                        this.statusUpdate_ListField = value;
                    }
                }
            }

            /// <remarks/>
            [System.SerializableAttribute()]
            [System.ComponentModel.DesignerCategoryAttribute("code")]
            [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
            public partial class UpdateContractStatusCredential
            {

                private string passwordField;

                private string userNameField;

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
                public string Password
                {
                    get
                    {
                        return this.passwordField;
                    }
                    set
                    {
                        this.passwordField = value;
                    }
                }

                /// <remarks/>
                [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
                public string UserName
                {
                    get
                    {
                        return this.userNameField;
                    }
                    set
                    {
                        this.userNameField = value;
                    }
                }
            }


        
    }
}
