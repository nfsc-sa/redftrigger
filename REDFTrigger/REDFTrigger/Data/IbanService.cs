﻿
namespace REDFTrigger.Data
{
    public class IbanServiceReq
    {

        // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
        public partial class Envelope
        {

           
            private EnvelopeHeader headerField;

            private EnvelopeBody bodyField;

            /// <remarks/>
            public EnvelopeHeader Header
            {
                get
                {
                    return this.headerField;
                }
                set
                {
                    this.headerField = value;
                }
            }

          
            /// <remarks/>
            public EnvelopeBody Body
            {
                get
                {
                    return this.bodyField;
                }
                set
                {
                    this.bodyField = value;
                }
            }
        }
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        public partial class EnvelopeHeader
        {

            private string actionField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.w3.org/2005/08/addressing")]
            public string Action
            {
                get
                {
                    return this.actionField;
                }
                set
                {
                    this.actionField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance", IsNullable = false)]
        public partial class BanksIBAN
        {

            private string aS_SERIALField;

            private string bANK_CODEField;

            private string fULL_NAME_ENField;

            private string iBANField;

            private string nIDField;

            /// <remarks/>
            public string AS_SERIAL
            {
                get
                {
                    return this.aS_SERIALField;
                }
                set
                {
                    this.aS_SERIALField = value;
                }
            }

            /// <remarks/>
            public string BANK_CODE
            {
                get
                {
                    return this.bANK_CODEField;
                }
                set
                {
                    this.bANK_CODEField = value;
                }
            }

            /// <remarks/>
            public string FULL_NAME_EN
            {
                get
                {
                    return this.fULL_NAME_ENField;
                }
                set
                {
                    this.fULL_NAME_ENField = value;
                }
            }

            /// <remarks/>
            public string IBAN
            {
                get
                {
                    return this.iBANField;
                }
                set
                {
                    this.iBANField = value;
                }
            }

            /// <remarks/>
            public string NID
            {
                get
                {
                    return this.nIDField;
                }
                set
                {
                    this.nIDField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
        public partial class EnvelopeBody
        {

            private insertIBANBanks insertIBANBanksField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://tempuri.org/")]
            public insertIBANBanks insertIBANBanks
            {
                get
                {
                    return this.insertIBANBanksField;
                }
                set
                {
                    this.insertIBANBanksField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
        public partial class insertIBANBanks
        {

            private insertIBANBanksCredential credentialField;

            private BanksIBAN[] iBAN_ListField;

            /// <remarks/>
            public insertIBANBanksCredential credential
            {
                get
                {
                    return this.credentialField;
                }
                set
                {
                    this.credentialField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("BanksIBAN", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance", IsNullable = false)]
            public BanksIBAN[] IBAN_List
            {
                get
                {
                    return this.iBAN_ListField;
                }
                set
                {
                    this.iBAN_ListField = value;
                }
            }
        }

        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://tempuri.org/")]
        public partial class insertIBANBanksCredential
        {

            private string passwordField;

            private string userNameField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
            public string Password
            {
                get
                {
                    return this.passwordField;
                }
                set
                {
                    this.passwordField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
            public string UserName
            {
                get
                {
                    return this.userNameField;
                }
                set
                {
                    this.userNameField = value;
                }
            }
        }


    }
}
		

