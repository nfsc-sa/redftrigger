﻿
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace REDFTrigger.Data.Rs
{
	[XmlRoot(ElementName = "Action", Namespace = "http://www.w3.org/2005/08/addressing")]
	public class Action
	{
		[XmlAttribute(AttributeName = "mustUnderstand", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public string MustUnderstand { get; set; }
		[XmlText]
		public string Text { get; set; }
	}

	[XmlRoot(ElementName = "Header", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class Header
	{
		[XmlElement(ElementName = "Action", Namespace = "http://www.w3.org/2005/08/addressing")]
		public Action Action { get; set; }
	}

	[XmlRoot(ElementName = "insertPaidResult", Namespace = "http://tempuri.org/")]
	public class InsertPaidResult
	{
		[XmlElement(ElementName = "Result", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string Result { get; set; }
		[XmlElement(ElementName = "Value", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string Value { get; set; }
		[XmlElement(ElementName = "ex_Description", Namespace = "http://schemas.datacontract.org/2004/07/RedfFinance")]
		public string Ex_Description { get; set; }
		[XmlAttribute(AttributeName = "b", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string B { get; set; }
		[XmlAttribute(AttributeName = "i", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string I { get; set; }
	}

	[XmlRoot(ElementName = "insertPaidResponse", Namespace = "http://tempuri.org/")]
	public class InsertPaidResponse
	{
		[XmlElement(ElementName = "insertPaidResult", Namespace = "http://tempuri.org/")]
		public InsertPaidResult InsertPaidResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class Body
	{
		[XmlElement(ElementName = "insertPaidResponse", Namespace = "http://tempuri.org/")]
		public InsertPaidResponse InsertPaidResponse { get; set; }
	}

	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class Envelope
	{
		[XmlElement(ElementName = "Header", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public Header Header { get; set; }
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public Body Body { get; set; }
		[XmlAttribute(AttributeName = "s", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string S { get; set; }
		[XmlAttribute(AttributeName = "a", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string A { get; set; }
	}

}
