﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace REDFTrigger.Data
{
    public class GetSupportRs
    {
		// using System.Xml.Serialization;
		// XmlSerializer serializer = new XmlSerializer(typeof(Envelope));
		// using (StringReader reader = new StringReader(xml))
		// {
		//    var test = (Envelope)serializer.Deserialize(reader);
		// }

		[XmlRoot(ElementName = "Action")]
		public class Action
		{

			[XmlAttribute(AttributeName = "mustUnderstand")]
			public int MustUnderstand { get; set; }

			[XmlText]
			public string Text { get; set; }
		}

		[XmlRoot(ElementName = "Header")]
		public class Header
		{

			[XmlElement(ElementName = "Action")]
			public Action Action { get; set; }
		}

		[XmlRoot(ElementName = "RealEstateSupportPaid")]
		public class RealEstateSupportPaid
		{

			[XmlElement(ElementName = "AS_SERIAL")]
			public string ASSERIAL { get; set; }

			[XmlElement(ElementName = "FULL_NAME_EN")]
			public string FULLNAMEEN { get; set; }

			[XmlElement(ElementName = "IBAN")]
			public object IBAN { get; set; }

			[XmlElement(ElementName = "IBAN_BANK_NAME")]
			public object IBANBANKNAME { get; set; }

			[XmlElement(ElementName = "NID")]
			public string NID { get; set; }

			[XmlElement(ElementName = "REPAY_NUM")]
			public int REPAYNUM { get; set; }

			[XmlElement(ElementName = "SUPPORT_VALUE")]
			public double SUPPORTVALUE { get; set; }

			[XmlElement(ElementName = "TRANSFER_APPROVAL_DATE_M")]
			public string TRANSFERAPPROVALDATEM { get; set; }
		}

		[XmlRoot(ElementName = "ResSuppPaidList")]
		public class ResSuppPaidList
		{

			[XmlElement(ElementName = "RealEstateSupportPaid")]
			public List<RealEstateSupportPaid> RealEstateSupportPaid { get; set; }
		}

		[XmlRoot(ElementName = "ResponseCode")]
		public class ResponseCode
		{

			[XmlElement(ElementName = "Result")]
			public string Result { get; set; }

			[XmlElement(ElementName = "Value")]
			public int Value { get; set; }

			[XmlElement(ElementName = "ex_Description")]
			public string ExDescription { get; set; }
		}

		[XmlRoot(ElementName = "getSupportPaidResult")]
		public class GetSupportPaidResult
		{

			[XmlElement(ElementName = "ResSuppPaidList")]
			public ResSuppPaidList ResSuppPaidList { get; set; }

			[XmlElement(ElementName = "ResponseCode")]
			public ResponseCode ResponseCode { get; set; }

			[XmlAttribute(AttributeName = "b")]
			public string B { get; set; }

			[XmlAttribute(AttributeName = "i")]
			public string I { get; set; }

			[XmlText]
			public string Text { get; set; }
		}

		[XmlRoot(ElementName = "getSupportPaidResponse")]
		public class GetSupportPaidResponse
		{

			[XmlElement(ElementName = "getSupportPaidResult")]
			public GetSupportPaidResult GetSupportPaidResult { get; set; }

			[XmlAttribute(AttributeName = "xmlns")]
			public string Xmlns { get; set; }

			[XmlText]
			public string Text { get; set; }
		}

		[XmlRoot(ElementName = "Body")]
		public class Body
		{

			[XmlElement(ElementName = "getSupportPaidResponse")]
			public GetSupportPaidResponse GetSupportPaidResponse { get; set; }
		}

		[XmlRoot(ElementName = "Envelope")]
		public class Envelope
		{

			[XmlElement(ElementName = "Header")]
			public Header Header { get; set; }

			[XmlElement(ElementName = "Body")]
			public Body Body { get; set; }

			[XmlAttribute(AttributeName = "s")]
			public string S { get; set; }

			[XmlAttribute(AttributeName = "a")]
			public string A { get; set; }

			[XmlText]
			public string Text { get; set; }
		}


	}
}