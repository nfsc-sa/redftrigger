﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REDFTrigger.Models
{
   
    class REDFContractStatus
    {
        public Credential Credential { get; set; }
        public string AS_SERIAL { get; set; }
        public string NID { get; set; }
        public string Contract_Status { get; set; }
        public string Status_REASON { get; set; }
        public string Status_Date { get; set; }

    }
    public class Credential
    {

        
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
