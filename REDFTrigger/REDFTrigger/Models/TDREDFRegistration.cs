﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REDFTrigger.Models
{
    public class TDREDFRegistration
    {
        public long ID { get; set; }
        public string REDFRefNumber { get; set; }
        public string TitleDeedNo { get; set; }
        public DateTime TransferDate { get; set; }
        public string RealEstateSubDivisionNumber { get; set; }
        public string LandNumber { get; set; }
        public string AreaOfLand { get; set; }
        public string AreaOfProperty { get; set; }
        public string PropertyTypeID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
