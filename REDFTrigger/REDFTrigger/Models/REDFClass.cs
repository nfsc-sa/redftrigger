﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REDFTrigger.Models
{
    public class REDFClass
    {
        public int Id { get; set; }
        public string REDFRefNumber { get; set; }
        public string NationalID { get; set; }
        public string InstallmentAmount { get; set; }
        public string TransactionDate { get; set; }
        public string InstallmentNo { get; set; }

    }
    public class REDFIban
    {
       
        public long REDFTransactionid { get; set; }
        public decimal AS_SERIAL { get; set; }

        public string FULL_NAME_EN { get; set; }
        public decimal NID { get; set; }
        public string BANK_CODE { get; set; }
        public string IBAN { get; set; }

    }
}