          
-- Author:  <Author,,Name>            
-- Create date: <Create Date,,>            
-- Description: <Description,,>            
-- exec GetContractDetail '183475cb-100d-4b4c-b37d-084dadf83bcf'            
-- =============================================            
Alter PROCEDURE [dbo].[GetContractDetail]            
 @contractId varchar(50)          
AS            
    BEGIN      
    SELECT  Distinct          
       
  IsPaid, cont.MortgageLoanAccountNumber,bpp.InstallmentNo,bpp.InstallmentDate,bpp.InstallmentAmount,bpp.InterestPaid,bpp.CapitalPaid,bpp.OutstandingPrincipal as CapitalOutStanding,      
  CASE      
    WHEN (bpp.InstallmentDate< GETDATE() or IsPaid = 1 )THEN (bpp.InstallmentAmount-(COALESCE(PA.PaidPrincipal,0) + COALESCE(PA.PaidTermCost,0)))      
    ELSE (null)      
END AS Remaining      
    ,  
 CASE      
    WHEN (bpp.InstallmentDate< GETDATE() and (IsPaid = 0 or IsPaid is null)) THEN (-1)    
 WHEN(RT.Status=2  and Rss.TRANSFER_APPROVAL_DATE_M < GETDATE() ) then 1  
    ELSE (RT.Status)      
END AS IsREDFNotified      
  ,  --RT.Status as IsREDFNotified,      
  p.LMSReferenceNo as ReferenceNo , cont.TotalPayableAmount,cont.OriginationDate,cont.FirstPaymentDate ,bpp.ID        
        FROM (select * from Contracts where ContractID = @contractId)  cont            
             INNER JOIN Beneficiary b ON cont.BeneficiaryID = b.BeneficiaryID            
             INNER JOIN property p ON cont.PropertyID = p.PropertyID            
  left join NGOMaster n on cont.ngoid = n.id            
             --INNER JOIN PropertyType pt ON p.PropertyType = pt.PropertyTypeID            
             INNER JOIN BeneficiaryPropertyPayment bpp ON cont.ContractID = bpp.ContractID            
  LEFT JOIN PaymentAllocation PA on PA.LMSReferenceNo = p.LMSReferenceNo and PA.InstallmentDate =bpp.InstallmentDate        
  left join (select * from REDFTransaction where ContractID =@contractId)RT on RT.ContractID=cont.ContractID and pa.InstallmentNo=RT.InstallmentNo and PA.InstallmentAmount =RT.InstallmentAmount          
     left join REDFSubsidyPaidReq RSP on RSP.REDFTransactionid= Rt.ID left join REDFSubsidyPaidRs Rss on RSP.id=Rss.REDFSubsidyReqId   
    
    --where cont.ContractID = @contractId           
    order by bpp.InstallmentNo;            
      
               
   --SELECT             
   --select  0.00 as MortgageLoanAccountNumber,0 as InstallmentNo,GETDATE() as DueDate,0 as InstallmentAmount,0 as InterestPaid,0 as InstallmentPaid,0 as CapitalPaid,0 as CapitalOutStanding          
    END; 