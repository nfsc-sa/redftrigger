USE [LoanMgmtSystem]
GO

ALTER TABLE REDFTransaction
ADD PRIMARY KEY (ID);
Go
/****** Object:  Table [dbo].[REDFSubsidyPaidReq]    Script Date: 8/24/2021 2:16:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REDFSubsidyPaidReq](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AS_SERIAL] [varchar](150) NULL,
	[REPAY_NUM] [varchar](50) NULL,
	[REDFTransactionid] [bigint] NULL,
	[createdOn] [datetime] NULL,
 CONSTRAINT [PK_REDFSubsidyPaidReq] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[REDFSubsidyPaidRs]    Script Date: 8/24/2021 2:16:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REDFSubsidyPaidRs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Result] [nvarchar](200) NULL,
	[Value] [int] NULL,
	[ex_Description] [nvarchar](max) NULL,
	[AS_SERIAL] [nvarchar](200) NULL,
	[IBAN] [nchar](10) NULL,
	[IBAN_BANK_NAME] [nchar](10) NULL,
	[NID] [nchar](10) NULL,
	[SUPPORT_VALUE] [decimal](18, 2) NULL,
	[TRANSFER_APPROVAL_DATE_M] [date] NULL,
	[REDFSubsidyReqId] [int] NULL,
	[createdOn] [datetime] NULL,
 CONSTRAINT [PK_REDFSubsidyPaidRs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[REDFSubsidyPaidReq] ADD  CONSTRAINT [DF_redfSub_createdOn]  DEFAULT (getdate()) FOR [createdOn]
GO
ALTER TABLE [dbo].[REDFSubsidyPaidRs] ADD  CONSTRAINT [DF_reffsp_createdOn]  DEFAULT (getdate()) FOR [createdOn]
GO
ALTER TABLE [dbo].[REDFSubsidyPaidReq]  WITH CHECK ADD FOREIGN KEY([REDFTransactionid])
REFERENCES [dbo].[REDFTransaction] ([ID])
GO
ALTER TABLE [dbo].[REDFSubsidyPaidRs]  WITH CHECK ADD FOREIGN KEY([REDFSubsidyReqId])
REFERENCES [dbo].[REDFSubsidyPaidReq] ([id])
GO
/****** Object:  StoredProcedure [dbo].[GetListForREDF]    Script Date: 8/24/2021 2:16:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
              
-- Author:  <diva smriti>              
-- Create date: <18-6-21 >              
-- Description: <fetch list for REDF api>              
-- exec [GetListForREDF]         
-- =============================================              
Alter PROCEDURE [dbo].[GetListForREDF]              
         
AS              
    BEGIN           
        
    SELECT RT.Id, B.NIDTypeID        
     ,B.NationalID        
    ,RT.TransactionID,        
    RT.InstallmentNo,        
    RT.InstallmentAmount,        
    RT.TransactionAmount,C.MortgageLoanAccountNumber,RT.TransactionType,        
    FORMAT(RT.TransactionDate, 'dd-MM-yyyy') as TransactionDate  from REDFTransaction RT left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID     
 left join Property PP on PP.PropertyID=C.PropertyID     
 inner join PaymentAllocation PA on PP.LMSReferenceNo=PA.LMSReferenceNo and RT.InstallmentNo= PA.InstallmentNo    
 where   PA.IsPaid=1    
 --- 3 retry  
 and(  
 ( RT.ID not in ( select REDFTransactionId from REDFInsertPaidRs  group by  REDFTransactionId having count(REDFTransactionId) >=3 ) and RT.Status= -1 )   
 or    
 RT.status is null  
 )  
      END; 
Go
/****** Object:  StoredProcedure [dbo].[GetREDFSubsidyPaid]    Script Date: 8/24/2021 2:16:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE GetREDFSubsidyPaid                      
         (        
   @daycount int =0)        
AS                      
    BEGIN      
  select * from         
    ( select RT.ID ,C.MortgageLoanAccountNumber,RT.InstallmentNo            
 from REDFTransaction   RT              
left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID               
 where           
RT.Status=1   )FT        
 left join (        
 select CreatedOn, Id, REDFTransactionId from REDFInsertPaidRs  where  DATEADD(DAY, @daycount, CreatedOn) <= GETDATE() and id  in         
(select min(id) from [dbo].[REDFInsertPaidRs]  group by  REDFTransactionId)        
) IPR         
 on FT.ID=IPR.REDFTransactionId        
  
    --- process subsidy paid after @daycount days      
      
      
  --To check 3 retry      
 --and FT.ID not in ( select REDFTransactionId from REDFSubsidyPaidRs SPR inner join REDFSubsidyPaidReq SPRq on SPRq.id=SPR.REDFSubsidyReqId   group by  REDFTransactionId having count(REDFTransactionId) >=3       
--)      
      END; 
GO
/****** Object:  StoredProcedure [dbo].[InsertREDFSubsidyPaidReq]    Script Date: 8/24/2021 2:16:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <diva,,smriti>  
-- Create date: <28_8,>  
-- Description: <REDF  SubsidyPaid request,,>  
-- =============================================  
CREATE PROCEDURE [dbo].[InsertREDFSubsidyPaidReq]  
 -- Add the parameters for the stored procedure here  
 @MortgageLoanAccountNumber varchar(50),   
 @InstallmentNo  varchar(50)  ,
 @id bigint
 AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 insert into [dbo].[REDFSubsidyPaidReq]  (AS_SERIAL,REPAY_NUM,REDFTransactionid) values(@MortgageLoanAccountNumber,@InstallmentNo,@Id)  
 select @@IDENTITY  
END  
GO
/****** Object:  StoredProcedure [dbo].[InsertSubsidyPaidRs]    Script Date: 8/24/2021 2:16:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================              
-- Author:  <diva,,smriti>              
-- Create date: <2_7,>              
-- Description: <REDF GET Support response,,>              
-- [InsertSubsidyPaidRs] '','','','','','','','','',''          
-- =============================================              
CREATE PROCEDURE [dbo].[InsertSubsidyPaidRs]              
 -- Add the parameters for the stored procedure here              
 @Result nvarchar(200) null,            
 @Value nvarchar(200) null,            
 @ex_Description nvarchar(200) null,            
 @AS_SERIAL nvarchar(200) null,            
 @IBAN nvarchar(200) null            
 ,@IBAN_BANK_NAME nvarchar(200) null            
 ,@NID nvarchar(200) null            
 ,@SUPPORT_VALUE decimal(18,2) null            
 ,@TRANSFER_APPROVAL_DATE_M date=null            
 ,@RequestId bigint null            
 AS              
BEGIN              
--alter table [REDFGetSupportRs] add IBAN_BANK_NAME nvarchar(200) null              
insert into [REDFSubsidyPaidRs] (Result,Value,ex_Description,AS_SERIAL,IBAN,IBAN_BANK_NAME,NID,SUPPORT_VALUE,TRANSFER_APPROVAL_DATE_M,REDFSubsidyReqId)               
values(@Result,@Value,@ex_Description,@AS_SERIAL,@IBAN,@IBAN_BANK_NAME,@NID,@SUPPORT_VALUE,@TRANSFER_APPROVAL_DATE_M,@RequestId)             
             
 -- SET NOCOUNT ON added to prevent extra result sets from              
 -- interfering with SELECT statements.              
              
  if(@Value= '1000' and @TRANSFER_APPROVAL_DATE_M is not null)            
   begin            
            
   update REDF                
     SET REDF.Status=2               
from REDFTransaction REDF  where id= (select REDFTransactionid  from REDFSubsidyPaidReq where id=@RequestId)            
   end            
--   else            
--   begin            
--     update REDF                
--     SET REDF.Status= -1               
--from REDFTransaction REDF  where id= (select REDFTransactionid  from REDFSubsidyPaidReq where id=@RequestId)              
               
--   end            
               
END 
GO