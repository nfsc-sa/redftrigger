USE [LoanMgmtSystem]
GO
/****** Object:  StoredProcedure [dbo].[GetListForIban]    Script Date: 9/3/2021 3:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
          ALTER TABLE [dbo].[Contracts]
	ADD REDFRefNumber NUMERIC(20)
    Go

-- Author:  <diva smriti>                      
-- Create date: <30-8-21 >                      
-- Description: <fetch list for REDF api>                      
-- exec [GetListForREDF]                 
-- =============================================                      
Alter PROCEDURE [dbo].[GetListForIban]                      
                 
AS                      
    BEGIN                   
                
    SELECT RT.Id  as REDFTransactionid              
     ,ISNULL(B.NationalID,0) as NID   ,ISNULL(RTRIM(LTRIM(        
        CONCAT(COALESCE(B.FirstName + ' ', '')        
            , COALESCE(B.MiddleName + ' ', '')        
            , COALESCE(B.Lastname, '')       
        )        
    )),'')  as FULL_NAME_EN ,ISNULL(C.REDFRefNumber,0) as AS_SERIAL, IsNULL(B.BANKCODE,'') as BANK_CODE,IsNULL(B.IBAN,'') as 
	IBAN from REDFTransaction RT left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID             
 left join Property PP on PP.PropertyID=C.PropertyID             
 inner join PaymentAllocation PA on PP.LMSReferenceNo=PA.LMSReferenceNo and RT.InstallmentNo= PA.InstallmentNo            
 where   PA.IsPaid=1    and RT.status is null            
       
       
      END;       
GO
/****** Object:  StoredProcedure [dbo].[GetListForREDF]    Script Date: 9/3/2021 3:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                  
-- Author:  <diva smriti>                  
-- Create date: <18-6-21 >                  
-- Description: <fetch list for REDF api>                  
-- exec [GetListForREDF]             
-- =============================================                  
Alter PROCEDURE [dbo].[GetListForREDF]                  
 (@RetryCount int=0) 
AS                  
    BEGIN               
            
    SELECT RT.Id, B.NIDTypeID            
     ,B.NationalID            
    ,RT.TransactionID,            
    RT.InstallmentNo,            
    RT.InstallmentAmount,            
    RT.TransactionAmount,C.REDFRefNumber,RT.TransactionType,            
    FORMAT(RT.TransactionDate, 'dd-MM-yyyy') as TransactionDate  from REDFTransaction RT left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID         
 left join Property PP on PP.PropertyID=C.PropertyID         
 inner join PaymentAllocation PA on PP.LMSReferenceNo=PA.LMSReferenceNo and RT.InstallmentNo= PA.InstallmentNo        
 where   PA.IsPaid=1        
 --- 3 retry      
 and(      
 ( RT.ID not in ( select REDFTransactionId from REDFInsertPaidRs  group by  REDFTransactionId having count(REDFTransactionId) >=@RetryCount ) and RT.Status= -1 )       
 or        
 RT.status =22      
 )      
      END; 
GO
/****** Object:  StoredProcedure [dbo].[GetREDFSubsidyPaid]    Script Date: 9/3/2021 3:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter PROCEDURE [dbo].[GetREDFSubsidyPaid]                      
         (        
   @daycount int =0)        
AS                      
    BEGIN      
  select * from         
    ( select RT.ID ,C.REDFRefNumber,RT.InstallmentNo            
 from REDFTransaction   RT              
left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID               
 where           
RT.Status=1   )FT        
 left join (        
 select CreatedOn, Id, REDFTransactionId from REDFInsertPaidRs  where  DATEADD(DAY, @daycount, CreatedOn) <= GETDATE() and id  in         
(select min(id) from [dbo].[REDFInsertPaidRs]  group by  REDFTransactionId)        
) IPR         
 on FT.ID=IPR.REDFTransactionId        
  
    --- process subsidy paid after @daycount days      
      
      
  --To check 3 retry      
 --and FT.ID not in ( select REDFTransactionId from REDFSubsidyPaidRs SPR inner join REDFSubsidyPaidReq SPRq on SPRq.id=SPR.REDFSubsidyReqId   group by  REDFTransactionId having count(REDFTransactionId) >=3       
--)      
      END; 
GO
