USE [LoanMgmtSystem]
GO
/****** Object:  Table [dbo].[REDFiBanReq]    Script Date: 9/2/2021 1:53:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REDFiBanReq](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AS_SERIAL] [varchar](150) NULL,
	[BANK_CODE] [varchar](50) NULL,
	[IBAN] [varchar](50) NULL,
	[FULL_NAME_EN] [varchar](50) NULL,
	[NID] [varchar](50) NULL,
	[REDFTransactionid] [bigint] NULL,
	[createdOn] [datetime] NULL,
 CONSTRAINT [PK_REDFiBanReq] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[REDFIbanResponse]    Script Date: 9/2/2021 1:53:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[REDFIbanResponse](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Result] [varchar](150) NULL,
	[Value] [varchar](150) NULL,
	[ex_Description] [varchar](max) NULL,
	[CreatedOn] [datetime] NULL,
	[response] [nvarchar](max) NULL,
	[RequestId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[REDFiBanReq] ADD  CONSTRAINT [DF_redfiban_createdOn]  DEFAULT (getdate()) FOR [createdOn]
GO
ALTER TABLE [dbo].[REDFIbanResponse] ADD  CONSTRAINT [df_REDFIbanResponse_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[REDFiBanReq]  WITH CHECK ADD FOREIGN KEY([REDFTransactionid])
REFERENCES [dbo].[REDFTransaction] ([ID])
GO
ALTER TABLE [dbo].[REDFiBanReq]  WITH CHECK ADD FOREIGN KEY([REDFTransactionid])
REFERENCES [dbo].[REDFTransaction] ([ID])
GO
ALTER TABLE [dbo].[REDFIbanResponse]  WITH CHECK ADD FOREIGN KEY([RequestId])
REFERENCES [dbo].[REDFiBanReq] ([id])
GO
/****** Object:  StoredProcedure [dbo].[GetListForIban]    Script Date: 9/2/2021 1:53:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
-- Author:  <diva smriti>                    
-- Create date: <30-8-21 >                    
-- Description: <fetch list for REDF api>                    
-- exec [GetListForREDF]               
-- =============================================                    
CREATE PROCEDURE [dbo].[GetListForIban]                    
               
AS                    
    BEGIN                 
              
    SELECT RT.Id  as REDFTransactionid            
     ,ISNULL(B.NationalID,0) as NID   ,ISNULL(RTRIM(LTRIM(      
        CONCAT(COALESCE(B.FirstName + ' ', '')      
            , COALESCE(B.MiddleName + ' ', '')      
            , COALESCE(B.Lastname, '')     
        )      
    )),'')  as FULL_NAME_EN ,ISNULL(C.MortgageLoanAccountNumber,0) as AS_SERIAL, IsNULL(B.BANKCODE,'') as BANK_CODE,IsNULL(B.IBAN,'') as IBAN from REDFTransaction RT left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID           
 left join Property PP on PP.PropertyID=C.PropertyID           
 inner join PaymentAllocation PA on PP.LMSReferenceNo=PA.LMSReferenceNo and RT.InstallmentNo= PA.InstallmentNo          
 where   PA.IsPaid=1    and RT.status is null          
     
     
      END;     
    
GO
/****** Object:  StoredProcedure [dbo].[GetListForREDF]    Script Date: 9/2/2021 1:53:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                
-- Author:  <diva smriti>                
-- Create date: <18-6-21 >                
-- Description: <fetch list for REDF api>                
-- exec [GetListForREDF]           
-- =============================================                
alter PROCEDURE [dbo].[GetListForREDF]                
           
AS                
    BEGIN             
          
    SELECT RT.Id, B.NIDTypeID          
     ,B.NationalID          
    ,RT.TransactionID,          
    RT.InstallmentNo,          
    RT.InstallmentAmount,          
    RT.TransactionAmount,C.MortgageLoanAccountNumber,RT.TransactionType,          
    FORMAT(RT.TransactionDate, 'dd-MM-yyyy') as TransactionDate  from REDFTransaction RT left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID       
 left join Property PP on PP.PropertyID=C.PropertyID       
 inner join PaymentAllocation PA on PP.LMSReferenceNo=PA.LMSReferenceNo and RT.InstallmentNo= PA.InstallmentNo      
 where   PA.IsPaid=1      
 --- 3 retry    
 and(    
 ( RT.ID not in ( select REDFTransactionId from REDFInsertPaidRs  group by  REDFTransactionId having count(REDFTransactionId) >=3 ) and RT.Status= -1 )     
 or      
 RT.status =22    
 )    
      END; 
GO
/****** Object:  StoredProcedure [dbo].[InsertIBanReq]    Script Date: 9/2/2021 1:53:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =============================================    
-- Author:  <diva,,smriti>    
-- Create date: <30_8,>    
-- Description: <REDF ibanservice response,,>   
-- =============================================    
CREATE PROCEDURE [dbo].[InsertIBanReq]    
 -- Add the parameters for the stored procedure here    
 (
 @AS_SERIAL varchar(150)=null ,
 @BANK_CODE varchar(50)=null,
 @IBAN varchar(50)=null,
 @FULL_NAME_EN varchar(50)=null,
 @NID varchar(50)=null
 ,@REDFTransactionid  Bigint )
 AS    
BEGIN    
--alter table [REDFGetSupportRs] add IBAN_BANK_NAME nvarchar(200) null    
insert into [dbo].[REDFiBanReq] (AS_SERIAL,BANK_CODE,IBAN,FULL_NAME_EN,NID,REDFTransactionid)     
values( @AS_SERIAL,@BANK_CODE,@IBAN,@FULL_NAME_EN,@NID,@REDFTransactionid ) 

 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
     select @@IDENTITY
     
    
END    
GO
/****** Object:  StoredProcedure [dbo].[Usp_InsertIbanResponse]    Script Date: 9/2/2021 1:53:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
-- Author:  <diva smriti>            
-- Create date: <21-6-21 >            
-- Description: <fetch list for REDF api>            
-- exec [GetListForREDF]       
-- =============================================            
CREATE PROCEDURE [dbo].[Usp_InsertIbanResponse]            
 ( @RequestId int,@Result varchar(150),@Value varchar(150),@ex_Description varchar(max),@response nvarchar(max))      
AS            
    BEGIN         
 declare @insertedId int;      
 insert into       
      
[dbo].[REDFIbanResponse] (RequestId,Result,Value,ex_Description,response)        
values(@RequestId,@Result,@Value,@ex_Description,@response)      
set @insertedId =  SCOPE_IDENTITY();      
if(@Value != '1000')      
begin       
 Update REDFTransaction set status=11 where ID in (select REDFTransactionid from REDFiBanReq where id=@RequestId);      
      
end      
else      
begin      
   Update REDFTransaction set status=22 where ID in (select REDFTransactionid from REDFiBanReq where id=@RequestId);      
       
 end      
      
      
 END; 
GO
