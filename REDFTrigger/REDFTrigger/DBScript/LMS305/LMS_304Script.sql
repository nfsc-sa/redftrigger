USE [LoanMgmtSystem]
GO
alter table REDFiBanReq alter column full_Name_en nvarchar(100)
GO

    
-- =============================================      
-- Author:  <diva,,smriti>      
-- Create date: <30_8,>      
-- Description: <REDF ibanservice response,,>     
-- =============================================      
Alter PROCEDURE [dbo].[InsertIBanReq]      
 -- Add the parameters for the stored procedure here      
 (  
 @AS_SERIAL varchar(150)=null ,  
 @BANK_CODE varchar(50)=null,  
 @IBAN varchar(50)=null,  
 @FULL_NAME_EN nvarchar(100)=null,  
 @NID varchar(50)=null  
 ,@REDFTransactionid  Bigint )  
 AS      
BEGIN      
--alter table [REDFGetSupportRs] add IBAN_BANK_NAME nvarchar(200) null      
insert into [dbo].[REDFiBanReq] (AS_SERIAL,BANK_CODE,IBAN,FULL_NAME_EN,NID,REDFTransactionid)       
values( @AS_SERIAL,@BANK_CODE,@IBAN,@FULL_NAME_EN,@NID,@REDFTransactionid )   
  
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
     select @@IDENTITY  
       
      
END

GO