USE [LoanMgmtSystem]
GO
/****** Object:  Trigger [dbo].[trg_product_Allocation]    Script Date: 9/8/2021 6:39:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================    
ALTER TRIGGER [dbo].[trg_product_Allocation]
ON [dbo].[PaymentTransaction]
AFTER INSERT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from    
	-- interfering with SELECT statements.    
	SET NOCOUNT ON;
	DECLARE @id VARCHAR(10) = ''
		   ,@installmentAmount DECIMAL(18, 2)
		   ,@AccountNumber VARCHAR(20) = ''
		   ,@Amount DECIMAL(18, 2) = 0
		   ,@PmtStatus VARCHAR(20) = ''
		   ,@NotificationRefId INT
		   ,@actualInstallmentDate DATETIME = GETDATE()
		   ,@actualInstallmentAmount DECIMAL(18, 2) = 0
		   ,@actualInterestPaid DECIMAL(18, 2) = 0
		   ,@actualCapitalPaid DECIMAL(18, 2) = 0
		   ,@DelinquentofInterest DECIMAL(18, 2) = 0
		   ,@Delinquentofprincipal DECIMAL(18, 2) = 0
		   ,@Interestdue DECIMAL(18, 2) = 0
		   ,@Principaldue DECIMAL(18, 2) = 0
		   ,@installmentNo BIGINT = 0
		   ,@isSystemGenerated bit = 0
		   ,@REDFSubsidized bit=0
		   ,@PaymentType VARCHAR(20)='',
		   @TransactionDate datetime,
        @TransactionAmount decimal(18,2),
		@ContractID uniqueidentifier,
       @PaymentTransactionID uniqueidentifier,
	   @transactionId bigint
		 --  ,@ContractId uniqueidentifier ;


	DECLARE @payment_allocation TABLE (
		InstallmentNo INT
	   ,InstallmentDate DATETIME
	   ,InstallmentAmount DECIMAL(18, 2)
	   ,PrincipalAmount DECIMAL(18, 2)
	   ,TermCost DECIMAL(18, 2)
	   ,PaidPrincipal DECIMAL(18, 2)
	   ,RemainingPrincipal DECIMAL(18, 2)
	   ,PaidTermCost DECIMAL(18, 2)
	   ,RemainingTermCost DECIMAL(18, 2)
	   ,IsPaid BIT
	   ,ReqId VARCHAR(20)
	   ,LMSReferenceNo VARCHAR(20)
	   ,AdvanceAmount decimal(18,2)
	)
	DECLARE @InstallmentInformationByLMSRef TABLE (
		Id INT IDENTITY (1, 1) NOT NULL
	   ,ContractID UNIQUEIDENTIFIER NOT NULL
	   ,Status BIT NULL
	   ,InstallmentNo BIGINT NULL
	   ,InstallmentDate DATETIME NULL
	   ,InstallmentAmount DECIMAL(18, 2) NULL
	   ,InterestPaid DECIMAL(18, 2) NULL
	   ,CapitalPaid DECIMAL(18, 2) NULL
	   ,CapitalOutStanding DECIMAL(18, 2) NULL
	   ,LMSReferenceNo NVARCHAR(20)
	   ,TotalPayableAmount NUMERIC NULL
	   ,OriginationDate DATETIME NULL
	   ,FirstPaymentDate DATETIME NULL
	)

	--set @ContractId=(select ContractID from  INSERTED )
	SELECT
		@AccountNumber = Prop.LMSReferenceNo,
		@isSystemGenerated = i.IsSystemGenerated,
		@REDFSubsidized=c.REDFSubsidized,
		@PaymentType=i.TransactionCategory,
		@TransactionDate=i.TransactionDate,
        @TransactionAmount=i.TransactionAmount,
		@ContractID=c.ContractID,
       @PaymentTransactionID=i.PaymentTransactionID,
	   @transactionId=i.TransactionID
	FROM INSERTED i
	JOIN [Contracts] C ON i.ContractID = C.ContractID
	JOIN [Property] Prop ON C.PropertyID = Prop.PropertyID

	SELECT @Amount = i.TransactionAmount FROM INSERTED i --WHERE i.TransactionType='RECEIPT'

	SET @NotificationRefId = '111'
	

	 SET  @Amount = @Amount+ ISNULL(( SELECT TOP 1 ISNULL(t.AdvanceAmount,0)         
  FROM PaymentAllocation t where LMSReferenceNo=@AccountNumber Order by InstallmentDate desc  ),0)
	--Settle amount first where IsPaid =0 for acccount    

	EXEC GetPaymentAllocationIsPaidFalse @LMSRef = @AccountNumber,@Amount = @Amount,@AmountOutput = @Amount OUTPUT;

	IF (@Amount > 0)
	BEGIN
		INSERT INTO @InstallmentInformationByLMSRef
			SELECT DISTINCT
				cont.ContractID
			   ,cont.Status
			   ,bpp.InstallmentNo
			   ,bpp.InstallmentDate
			   ,bpp.InstallmentAmount
			   ,bpp.InterestPaid
			   ,bpp.CapitalPaid
			   ,bpp.OutstandingPrincipal
			   ,Prop.LMSReferenceNo AS ReferenceNo
			   ,cont.TotalPayableAmount
			   ,cont.OriginationDate
			   ,cont.FirstPaymentDate
			FROM [Contracts] cont
			FULL JOIN [dbo].[BeneficiaryPropertyPayment] bpp ON cont.ContractID = bpp.ContractID
			LEFT JOIN [Property] Prop ON cont.PropertyID = Prop.PropertyID
			WHERE cont.ContractID IS NOT NULL AND cont.Status = 1 AND LMSReferenceNo = @AccountNumber
			AND NOT EXISTS (SELECT
					1
				FROM PaymentAllocation PA
				WHERE PA.InstallmentDate = bpp.InstallmentDate AND LMSReferenceNo = @AccountNumber
				AND IsPaid = 1) ORDER BY InstallmentNo ASC

		IF EXISTS (SELECT COUNT(1) FROM @InstallmentInformationByLMSRef)
		BEGIN
			DECLARE @InstallmentInfoVariableRowId INT
			SELECT TOP 1
				@actualInstallmentAmount = InstallmentAmount
			FROM @InstallmentInformationByLMSRef
			SELECT TOP 1
				@actualInterestPaid = InterestPaid
			FROM @InstallmentInformationByLMSRef
			SELECT TOP 1
				@actualCapitalPaid = CapitalPaid
			FROM @InstallmentInformationByLMSRef
			SELECT TOP 1
				@actualInstallmentDate = InstallmentDate
			FROM @InstallmentInformationByLMSRef
			SELECT TOP 1
				@installmentNo = InstallmentNo
			FROM @InstallmentInformationByLMSRef
			SELECT TOP 1
				@InstallmentInfoVariableRowId = Id
			FROM @InstallmentInformationByLMSRef
			IF (@actualInstallmentAmount = 0
				AND @actualInterestPaid = 0
				AND @actualCapitalPaid = 0)
			BEGIN
				WHILE (@actualInstallmentAmount = 0
				AND @actualInterestPaid = 0
				AND @actualCapitalPaid = 0)--IF(@actualInstallmentAmount =0 AND @actualInterestPaid =0 AND @actualCapitalPaid =0) 
				BEGIN
				INSERT INTO PaymentAllocation (InstallmentNo,
				InstallmentDate,
				InstallmentAmount,
				PrincipalAmount,
				TermCost,
				PaidPrincipal,
				RemainingPrincipal,
				PaidTermCost,
				RemainingTermCost,
				IsPaid,
				ReqId,
				LMSReferenceNo)
					VALUES (@installmentNo, @actualInstallmentDate, 0, 0, 0, 0, 0, 0, 0, 1, @NotificationRefId, @AccountNumber)
					--if(@REDFSubsidized=1 and  @PaymentType='NFSC SADAD')
					--begin
					--	insert into REDFTransaction (ContractID,InstallmentNo,InstallmentAmount,PaymentTransactionID,TransactionDate,TransactionID,
					--	TransactionAmount) values(@ContractID,@installmentNo,@installmentAmount,@PaymentTransactionID,@TransactionDate
					--	,@transactionId)
					--end


				DELETE FROM @InstallmentInformationByLMSRef
				WHERE Id = @InstallmentInfoVariableRowId



				SELECT TOP 1
					@actualInstallmentAmount = InstallmentAmount
				   ,@actualInterestPaid = InterestPaid
				   ,@actualCapitalPaid = CapitalPaid
				   ,@actualInstallmentDate = InstallmentDate
				   ,@installmentNo = InstallmentNo
				   ,@InstallmentInfoVariableRowId = Id
				FROM @InstallmentInformationByLMSRef
				WHERE Id != @InstallmentInfoVariableRowId
				ORDER BY InstallmentNo ASC
				IF ((SELECT
							COUNT(1)
						FROM @InstallmentInformationByLMSRef)
					= 0)
					BREAK;
				END
			END

		END


		DECLARE @result_1 DECIMAL
		IF (@actualInstallmentAmount != 0)
		BEGIN
			SET @result_1 = FLOOR((@Amount / @actualInstallmentAmount));
		END
		ELSE
		BEGIN
			SET @result_1 = 0;
		END
		DECLARE @getNextRow INT = 1
		SELECT TOP 1
			@getNextRow = Id
		FROM @InstallmentInformationByLMSRef
		DECLARE @i INT
		SET @i = 0
		IF (@result_1 >= 1) --6000>5000    
		BEGIN
			WHILE (@i < @result_1)
			BEGIN

			INSERT INTO @payment_allocation
				SELECT
					InstallmentNo
				   ,InstallmentDate
				   ,InstallmentAmount
				   ,CapitalPaid
				   ,InterestPaid
				   ,@actualCapitalPaid
				   ,0
				   ,@actualInterestPaid
				   ,0
				   ,1
				   ,@NotificationRefId
				   ,@AccountNumber
				   ,null
				FROM @InstallmentInformationByLMSRef
				WHERE Id = @getNextRow
			DELETE FROM @InstallmentInformationByLMSRef
			WHERE Id = @getNextRow --added 
			SET @i = @i + 1
			SET @getNextRow = @getNextRow + 1
			SET @Amount = @Amount - (@actualCapitalPaid + @actualInterestPaid);
			END
			SELECT TOP 1
				@actualInstallmentAmount = InstallmentAmount
			   ,@actualInterestPaid = InterestPaid
			   ,@actualCapitalPaid = CapitalPaid
			   ,@actualInstallmentDate = InstallmentDate
			   ,@installmentNo = InstallmentNo
			   ,@InstallmentInfoVariableRowId = Id
			FROM @InstallmentInformationByLMSRef
			WHERE Id = @getNextRow

			IF (@actualInstallmentAmount = 0
				AND @actualInterestPaid = 0
				AND @actualCapitalPaid = 0)
			BEGIN
				WHILE (@actualInstallmentAmount = 0
				AND @actualInterestPaid = 0
				AND @actualCapitalPaid = 0)--IF(@actualInstallmentAmount =0 AND @actualInterestPaid =0 AND @actualCapitalPaid =0) 
				BEGIN
				INSERT INTO PaymentAllocation (InstallmentNo,
				InstallmentDate,
				InstallmentAmount,
				PrincipalAmount,
				TermCost,
				PaidPrincipal,
				RemainingPrincipal,
				PaidTermCost,
				RemainingTermCost,
				IsPaid,
				ReqId,
				LMSReferenceNo)
					VALUES (@installmentNo, @actualInstallmentDate, 0, 0, 0, 0, 0, 0, 0, 1, @NotificationRefId, @AccountNumber)


				SET @getNextRow = @getNextRow + 1
				DELETE FROM @InstallmentInformationByLMSRef
				WHERE Id = @InstallmentInfoVariableRowId

				SELECT TOP 1
					@actualInstallmentAmount = InstallmentAmount
				   ,@actualInterestPaid = InterestPaid
				   ,@actualCapitalPaid = CapitalPaid
				   ,@actualInstallmentDate = InstallmentDate
				   ,@installmentNo = InstallmentNo
				   ,@InstallmentInfoVariableRowId = Id
				FROM @InstallmentInformationByLMSRef
				WHERE Id != @InstallmentInfoVariableRowId
				ORDER BY InstallmentNo ASC
				IF ((SELECT
							COUNT(1)
						FROM @InstallmentInformationByLMSRef)
					= 0)
					BREAK;
				END
			END
			IF (@Amount > 0
				AND @Amount < @actualInstallmentAmount)
			BEGIN
				SET @DelinquentofInterest = @actualInterestPaid--// 1500    
				SET @Delinquentofprincipal = @actualCapitalPaid--;//3500    

				IF (@Amount < @actualInterestPaid) --//1000<1500    
				BEGIN

					SET @Interestdue = @actualInterestPaid - @Amount; --// 1500 -(6000-5000) = 500    
					SET @Principaldue = @actualCapitalPaid;

					INSERT INTO @payment_allocation
						SELECT
							InstallmentNo
						   ,InstallmentDate
						   ,InstallmentAmount
						   ,CapitalPaid
						   ,InterestPaid
						   ,0
						   ,@Principaldue
						   ,@Amount
						   ,@Interestdue
						   ,0
						   ,@NotificationRefId
						   ,@AccountNumber
						    ,null
						FROM @InstallmentInformationByLMSRef
						WHERE Id = @getNextRow

				END
				ELSE
				BEGIN

					SET @Interestdue = 0; --// 2000-1500=500    
					SET @Amount = @Amount - @actualInterestPaid; --//2000-1500=500    
					SET @Principaldue = @actualCapitalPaid - @Amount; --// 3500-500=3000    

					INSERT INTO @payment_allocation
						SELECT
							InstallmentNo
						   ,InstallmentDate
						   ,InstallmentAmount
						   ,CapitalPaid
						   ,InterestPaid
						   ,(@actualCapitalPaid - @Principaldue)
						   ,@Principaldue
						   ,@actualInterestPaid
						   ,@Interestdue
						   ,0
						   ,@NotificationRefId
						   ,@AccountNumber
						    ,null
						FROM @InstallmentInformationByLMSRef
						WHERE Id = @getNextRow
				END

			END
			--insert in payment allocation    
			--IF EXISTS (SELECT
			--			COUNT(1)
			--		FROM @payment_allocation)
			--BEGIN
			--	INSERT INTO PaymentAllocation (InstallmentNo,
			--	InstallmentDate,
			--	InstallmentAmount,
			--	PrincipalAmount,
			--	TermCost,
			--	PaidPrincipal,
			--	RemainingPrincipal,
			--	PaidTermCost,
			--	RemainingTermCost,
			--	IsPaid,
			--	ReqId,
			--	LMSReferenceNo)
			--		SELECT
			--			InstallmentNo
			--		   ,InstallmentDate
			--		   ,InstallmentAmount
			--		   ,PrincipalAmount
			--		   ,TermCost
			--		   ,PaidPrincipal
			--		   ,RemainingPrincipal
			--		   ,PaidTermCost
			--		   ,RemainingTermCost
			--		   ,IsPaid
			--		   ,ReqId
			--		   ,LMSReferenceNo
			--		FROM @payment_allocation
			--END
		END
		ELSE
		BEGIN
			IF (@Amount > 0
				AND @Amount < @actualInstallmentAmount)
			BEGIN

				SET @DelinquentofInterest = @actualInterestPaid--;// 1500    
				SET @Delinquentofprincipal = @actualCapitalPaid--;//3500    
				IF (@Amount < @actualInterestPaid)-- //1000<1500    

				BEGIN

					SET @Interestdue = @actualInterestPaid - @Amount--; // 1500 -(6000-5000) = 500    
					SET @Principaldue = @actualCapitalPaid;
					INSERT INTO @payment_allocation
						SELECT
							InstallmentNo
						   ,InstallmentDate
						   ,InstallmentAmount
						   ,CapitalPaid
						   ,InterestPaid
						   ,0
						   ,@Principaldue
						   ,@Amount
						   ,@Interestdue
						   ,0
						   ,@NotificationRefId
						   ,@AccountNumber
						    ,null
						FROM @InstallmentInformationByLMSRef
						WHERE Id = @getNextRow

				END
				ELSE
				BEGIN
					SET @Interestdue = 0--; // 2000-1500=500    
					SET @Amount = @Amount - @actualInterestPaid--; //2000-1500=500    
					SET @Principaldue = @actualCapitalPaid - @Amount--;// 3500-500=3000    
					INSERT INTO @payment_allocation
						SELECT
							InstallmentNo
						   ,InstallmentDate
						   ,InstallmentAmount
						   ,CapitalPaid
						   ,InterestPaid
						   ,(@actualCapitalPaid - @Principaldue)
						   ,@Principaldue
						   ,@actualInterestPaid
						   ,@Interestdue
						   ,0
						   ,@NotificationRefId
						   ,@AccountNumber
						    ,null
						FROM @InstallmentInformationByLMSRef
						WHERE Id = @getNextRow

				END

			END



			
		END
--		declare @dueAmount decimal(18,5)
--		declare @duedate datetime
--		set @dueAmount =(select  sum(InstallmentAmount)
--FROM @payment_allocation
--WHERE IsPaid=0)
--set @duedate= (select min(InstallmentDate) FROM @payment_allocation
--WHERE IsPaid=0)
-- update Contracts set DueDate=@duedate,DueAmount=@dueAmount where [ContractID]=@ContractId



--EXEC DueValueCalculation @lmsRef =@AccountNumber
		--EXEC updateContractDetails @LMSReferenceNo = @AccountNumber
	declare @date datetime =  getdate()
				--declare @date datetime =  '2021-12-01 00:00:00.000'
		
	declare @LastpaidinstallmentDate datetime = (select top 1 InstallmentDate from @payment_allocation where IsPaid = 1 order by InstallmentNo desc)
  --select @LastpaidinstallmentDate;

  declare @AdvanceAmount1 DECIMAL(18,2) = (select Sum(PaidPrincipal) + SUM(PaidTermCost) 
  from @payment_allocation where  InstallmentDate > @date)

  --declare @ContractID UNIQUEIDENTIFIER = (select top 1 ContractID from Contracts where PropertyID = (select top 1 PropertyID from Property where LMSReferenceNo = @AccountNumber))
  --select @ContractID
 
   --select @LastInstallmentNo

   

  delete from @payment_allocation where InstallmentDate > @date; 
  --select @AdvanceAmount1

 
  --insert in payment allocation    
			IF EXISTS (SELECT
						COUNT(1)
					FROM @payment_allocation)
			BEGIN
				INSERT INTO PaymentAllocation (InstallmentNo,
				InstallmentDate,
				InstallmentAmount,
				PrincipalAmount,
				TermCost,
				PaidPrincipal,
				RemainingPrincipal,
				PaidTermCost,
				RemainingTermCost,
				IsPaid,
				ReqId,
				LMSReferenceNo)
					SELECT
						InstallmentNo
					   ,InstallmentDate
					   ,InstallmentAmount
					   ,PrincipalAmount
					   ,TermCost
					   ,PaidPrincipal
					   ,RemainingPrincipal
					   ,PaidTermCost
					   ,RemainingTermCost
					   ,IsPaid
					   ,ReqId
					   ,LMSReferenceNo
					FROM @payment_allocation

					if(@REDFSubsidized=1 and  (@PaymentType='NFSC SADAD' or @PaymentType='SADAD'))
					begin
						insert into REDFTransaction (ContractID,InstallamentDate,InstallmentNo,InstallmentAmount,PaymentTransactionID,TransactionDate,TransactionID,
						TransactionAmount ,status) select @ContractID,InstallmentDate,InstallmentNo,InstallmentAmount,@PaymentTransactionID,@TransactionDate
					,@transactionId,@TransactionAmount,0 FROM @payment_allocation
					end


			END

				declare @LastInstallmentNo bigint = (select top 1 InstallmentNo from PaymentAllocation where LMSReferenceNo = @AccountNumber order by InstallmentDate desc)

			update PaymentAllocation set AdvanceAmount = null where LMSReferenceNo = @AccountNumber

	   update PaymentAllocation set AdvanceAmount = @AdvanceAmount1 where InstallmentNo = @LastInstallmentNo  and LMSReferenceNo = @AccountNumber
	END

	--ELSE
	--BEGIN
	--	EXEC updateContractDetails @LMSReferenceNo = @AccountNumber
	--END
END

GO

            
 










            
 --ALTER TABLE [dbo].[Contracts]  
  --ADD REDFRefNumber NUMERIC(20)  
  --Go  
  
-- Author:  <diva smriti>                        
-- Create date: <30-8-21 >                        
-- Description: <fetch list for REDF api>                        
-- exec [GetListForREDF]                   
-- =============================================                        
Alter PROCEDURE [dbo].[GetListForIban]                        
                   
AS                        
    BEGIN                     
                  
    SELECT RT.Id  as REDFTransactionid                
     ,ISNULL(B.NationalID,0) as NID   ,ISNULL(RTRIM(LTRIM(          
        CONCAT(COALESCE(B.FirstName + ' ', '')          
            , COALESCE(B.MiddleName + ' ', '')          
            , COALESCE(B.Lastname, '')         
        )          
    )),'')  as FULL_NAME_EN ,ISNULL(C.REDFRefNumber,0) as AS_SERIAL, IsNULL(B.BANKCODE,'') as BANK_CODE,IsNULL(B.IBAN,'') as   
 IBAN from REDFTransaction RT left join Contracts C on C.ContractID=RT.ContractID left join Beneficiary B on B.BeneficiaryID=C.BeneficiaryID               
 left join Property PP on PP.PropertyID=C.PropertyID               
 inner join PaymentAllocation PA on PP.LMSReferenceNo=PA.LMSReferenceNo and RT.InstallmentNo= PA.InstallmentNo              
 where   PA.IsPaid=1    and RT.status =0              
         
         
      END;         

GO