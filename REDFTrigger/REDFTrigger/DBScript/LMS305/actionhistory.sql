USE [LoanMgmtSystem]
GO
insert into ActionGroupMaster (GroupInEn,IsActive) values ('REDF',1)
Go
insert into ActionMaster(ActionInEn,IsActive,IsDeleted,ActionName) values ('REDF',1,0,'Support amount of {0} has been transfered on {1}')
Go
update ActionMaster set ActionGroupID= (select top 1 id from ActionGroupMaster where GroupInEn='REDF') where ActionInEn='REDF'
Go
alter table actionmaster add ActionReferenceName varchar(50)
Go
update ActionMaster set actionReferenceName ='REDF' where ActionInEn='REDF'
Go


-- =============================================              
-- Author:  <diva,,smriti>              
-- Create date: <2_7,>              
-- Description: <REDF GET Support response,,>              
-- [InsertSubsidyPaidRs] '','','','','','','','','',''        
--Modified on :8_9
-- Added in actionHistory
-- =============================================              
alter PROCEDURE [dbo].[InsertSubsidyPaidRs]              
 -- Add the parameters for the stored procedure here              
 @Result nvarchar(200) null,            
 @Value nvarchar(200) null,            
 @ex_Description nvarchar(200) null,            
 @AS_SERIAL nvarchar(200) null,            
 @IBAN nvarchar(200) null            
 ,@IBAN_BANK_NAME nvarchar(200) null            
 ,@NID nvarchar(200) null            
 ,@SUPPORT_VALUE decimal(18,2) null            
 ,@TRANSFER_APPROVAL_DATE_M date=null            
 ,@RequestId bigint null            
 AS              
BEGIN              
--alter table [REDFGetSupportRs] add IBAN_BANK_NAME nvarchar(200) null              
insert into [REDFSubsidyPaidRs] (Result,Value,ex_Description,AS_SERIAL,IBAN,IBAN_BANK_NAME,NID,SUPPORT_VALUE,TRANSFER_APPROVAL_DATE_M,REDFSubsidyReqId)               
values(@Result,@Value,@ex_Description,@AS_SERIAL,@IBAN,@IBAN_BANK_NAME,@NID,@SUPPORT_VALUE,@TRANSFER_APPROVAL_DATE_M,@RequestId)             
             
 -- SET NOCOUNT ON added to prevent extra result sets from              
 -- interfering with SELECT statements.              
              
  if(@Value= '1000' and @TRANSFER_APPROVAL_DATE_M is not null)            
   begin            
  declare @RedfTransactionId bigint=  (select top 1 REDFTransactionid  from REDFSubsidyPaidReq where id=@RequestId); 
          
   update REDF                
     SET REDF.Status=2               
from REDFTransaction REDF  where id=  @RedfTransactionId    ;
declare @ActionId uniqueidentifier= (select top 1 ActionId from ActionMaster where actionReferenceName ='REDF' )
declare @contractId uniqueidentifier= (select top 1 contractid from REDFTransaction  where id=@RedfTransactionId )
declare @Description nvarchar(max)= (select top 1 REPLACE(REPLACE(ActionName,'{0}',@SUPPORT_VALUE),'{1}',convert(varchar, @TRANSFER_APPROVAL_DATE_M, 105))  from ActionMaster where actionReferenceName ='REDF')
declare @username uniqueidentifier=(select top 1 UserID from UserMaster  where UserName='SYSTEM' )
Insert into ContractActionHistory(ActionId,ContractID,PreformedBy,PreformedOn,Description,IsActive)
values(@ActionId,@contractId,@username,GETDATE(),@Description,1);

   end            
--   else            
--   begin            
--     update REDF                
--     SET REDF.Status= -1               
--from REDFTransaction REDF  where id= (select REDFTransactionid  from REDFSubsidyPaidReq where id=@RequestId)              
               
--   end            
               
END 
 Go

-- =============================================                
-- Author:  <diva,,smriti>                
-- Create date: <2_7,>                
-- Description: <Update status when server not able to connect>                
-- =============================================                
Create PROCEDURE [dbo].[Usp_UpdateREDFForFailedStatus]                
 -- Add the parameters for the stored procedure here                       
 @REDFTransactionId bigint null              
 AS                
BEGIN                
--alter table [REDFGetSupportRs] add IBAN_BANK_NAME nvarchar(200) null                
       update REDF                  
     SET REDF.Status= -1                 
from REDFTransaction REDF  where id=@REDFTransactionId               
                            
                 
END   

Go